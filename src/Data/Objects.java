package Data;

import javax.vecmath.Color3f;

public final class Objects {

    /* Constants and Variables */
    public static boolean stopFlag = false;

    /* Objects */
    public static UI.Custom.ProcessGraphsThread processGraphsThread = null;
    public static javax.swing.JProgressBar progressBar = null;
    public static javax.swing.JLabel statusBar = null;
    public static javax.media.j3d.Material material = new javax.media.j3d.Material(new Color3f(0.1f, 0.1f, 0.1f), new Color3f(0.0f, 0.0f, 0.0f), new Color3f(0.0f, 0.5f, 0.0f), new Color3f(0.1f, 0.1f, 0.1f), 0.1f);
    public static java.util.ArrayList<Core.Variable> globalVariables;
    public static java.util.ArrayList<Core.Matrix2D> globalMatrices2D;
    public static java.util.ArrayList<Core.Function> globalFunctions;
    public static java.util.ArrayList<Graph.Graph> graphs;
    public static java.util.ArrayList<UI.Custom.TextInternalFrame> textInternalFrames;
    public static java.util.ArrayList<UI.Custom.GraphInternalFrame> graphInternalFrames;
    public static UI.Custom.GraphSettingsFrame graphSettingsFrame;
    public static UI.HelpFrame helpFrame;
    public static javax.swing.JDesktopPane desktopPane;
    public static UI.MainFrame mainFrame;
}
