package Data;

import java.awt.Color;
import java.util.prefs.Preferences;

public final class Defaults {

    public static enum Graph2DType {

        CARTESIAN_PERSISTENT,
        CARTESIAN_DEFAULT,
        POLAR_PERSISTENT,
        POLAR_DEFAULT,
        POLAR_THREE_TO_ONE_PI_BY_TWO,
        POLAR_ZERO_TO_PI,
        POLAR_ZERO_TO_TWO_PI,
    }

    public static enum Graph3DType {

        CARTESIAN_PERSISTENT,
        CARTESIAN_DEFAULT,
        CYLINDRICAL_PERSISTENT,
        CYLINDRICAL_RHO,
        CYLINDRICAL_PHI,
        CYLINDRICAL_Z,
        SPHERICAL_PERSISTENT,
        SPHERICAL_R,
        SPHERICAL_THETA,
        SPHERICAL_PHI
    }

    public static class Graph2D {

        public static final double markerWidth = 4,
                pointSize = 1,
                minSeparation = 0.1;
        public static final Color markerColor = Color.BLACK, graphColor = Color.BLACK, axesColor = Color.BLACK;
        public static final double gridXSpacing = 1,
                gridYSpacing = 1,
                xScale = 100,
                yScale = 100,
                xOrigin = 360,
                yOrigin = 290;

        public static final Graph.Graph2D assignTemplate(Graph.Graph2D graph2D, Graph2DType type) {
            graph2D.setCoordinateSystemParameters(xOrigin, yOrigin, xScale, yScale, gridXSpacing, gridYSpacing);
            switch (type) {
                case CARTESIAN_DEFAULT:
                    Cartesian_Default_2D.assignTemplate(graph2D);
                    break;
                case POLAR_DEFAULT:
                    Polar_Default_2D.assignTemplate(graph2D);
                    break;
                case POLAR_THREE_TO_ONE_PI_BY_TWO:
                    Polar_Three_To_One_PI_By_Two_2D.assignTemplate(graph2D);
                    break;
                case POLAR_ZERO_TO_PI:
                    Polar_Zero_To_PI_2D.assignTemplate(graph2D);
                    break;
                case POLAR_ZERO_TO_TWO_PI:
                    Polar_Zero_To_Two_PI_2D.assignTemplate(graph2D);
                    break;
            }
            return graph2D;
        }

        public static final Graph.Graph2D assignPersistentTemplate(Graph.Graph2D graph2D, Graph2DType type) {
            switch (type) {
                case CARTESIAN_PERSISTENT:
                    return Cartesian_Persistent_2D.assignTemplate(graph2D);
                case POLAR_PERSISTENT:
                    return Polar_Persistent_2D.assignTemplate(graph2D);
            }
            return graph2D;
        }
    }

    public static class Graph3D {

        public static final double markerWidth = 4,
                pointSize = 1,
                minSeparation = 0.1;
        public static final double gridXSpacing = 1,
                gridYSpacing = 1,
                gridZSpacing = 1,
                xScale = 1,
                yScale = 1,
                zScale = 1,
                xOrigin = 0,
                yOrigin = 0,
                zOrigin = 0;

        /* SET METHODS */
        public static final Graph.Graph3D assignTemplate(Graph.Graph3D graph3D, Graph3DType type) {
            graph3D.setCoordinateSystemParameters(xOrigin, yOrigin, zOrigin, xScale, yScale, zScale, gridXSpacing, gridYSpacing, gridZSpacing);
            switch (type) {
                case CARTESIAN_DEFAULT:
                    Cartesian_Default_3D.assignTemplate(graph3D);
                    break;
                case CYLINDRICAL_RHO:
                    Cylindrical_Rho_3D.assignTemplate(graph3D);
                    break;
                case CYLINDRICAL_PHI:
                    Cylindrical_Phi_3D.assignTemplate(graph3D);
                    break;
                case CYLINDRICAL_Z:
                    Cylindrical_Z_3D.assignTemplate(graph3D);
                    break;
                case SPHERICAL_R:
                    Spherical_R_3D.assignTemplate(graph3D);
                    break;
                case SPHERICAL_THETA:
                    Spherical_Theta_3D.assignTemplate(graph3D);
                    break;
                case SPHERICAL_PHI:
                    Spherical_Phi_3D.assignTemplate(graph3D);
                    break;
            }
            return graph3D;
        }

        public static final Graph.Graph3D assignPersistentTemplate(Graph.Graph3D graph3D, Graph3DType type) {
            switch (type) {
                case CARTESIAN_PERSISTENT:
                    return Cartesian_Persistent_3D.assignTemplate(graph3D);
                case CYLINDRICAL_PERSISTENT:
                    return Cylindrical_Persistent_3D.assignTemplate(graph3D);
                case SPHERICAL_PERSISTENT:
                    return Spherical_Persistent_3D.assignTemplate(graph3D);
            }
            return graph3D;
        }
    }
}

/* Persistent defaults */
final class Cartesian_Persistent_2D extends Defaults.Graph2D {

    public static final Graph.Graph2D assignTemplate(Graph.Graph2D graph2D) {
        Preferences prefs = Preferences.userRoot().node("EmeraldMath/graph2D/cartesian");
        graph2D.setCoordinateSystemParameters(prefs.getDouble("xOrigin", 360), prefs.getDouble("yOrigin", 290), prefs.getDouble("xScale", 100), prefs.getDouble("yScale", 100), prefs.getDouble("xSpacing", 1), prefs.getDouble("ySpacing", 1));
        graph2D.setPlottingParameters(prefs.getDouble("minI", -4), prefs.getDouble("maxI", 4), prefs.getDouble("minD", -4), prefs.getDouble("maxD", 4), prefs.getDouble("limI", 1000), prefs.getDouble("limD", 1000), prefs.getDouble("density", 5));
        return graph2D;
    }
}

final class Polar_Persistent_2D extends Defaults.Graph2D {

    public static final Graph.Graph2D assignTemplate(Graph.Graph2D graph2D) {
        Preferences prefs = Preferences.userRoot().node("EmeraldMath/graph2D/polar");
        graph2D.setCoordinateSystemParameters(prefs.getDouble("xOrigin", 360), prefs.getDouble("yOrigin", 290), prefs.getDouble("xScale", 100), prefs.getDouble("yScale", 100), prefs.getDouble("xSpacing", 1), prefs.getDouble("ySpacing", 1));
        graph2D.setPlottingParameters(prefs.getDouble("minI", -2 * Math.PI), prefs.getDouble("maxI", 2 * Math.PI), prefs.getDouble("minD", -5), prefs.getDouble("maxD", 5), prefs.getDouble("limI", 1000), prefs.getDouble("limD", 1000), prefs.getDouble("density", 50));
        return graph2D;
    }
}

final class Cartesian_Persistent_3D extends Defaults.Graph3D {

    public static final Graph.Graph3D assignTemplate(Graph.Graph3D graph3D) {
        Preferences prefs = Preferences.userRoot().node("EmeraldMath/graph3D/cartesian");
        graph3D.setCoordinateSystemParameters(prefs.getDouble("xOrigin", 0), prefs.getDouble("yOrigin", 0), prefs.getDouble("zOrigin", 0), prefs.getDouble("xScale", 1), prefs.getDouble("yScale", 1), prefs.getDouble("zScale", 1), prefs.getDouble("xSpacing", 1), prefs.getDouble("ySpacing", 1), prefs.getDouble("zSpacing", 1));
        graph3D.setPlottingParameters(prefs.getDouble("minI1", -3), prefs.getDouble("maxI1", 3), prefs.getDouble("minI2", -3), prefs.getDouble("maxI2", 3), prefs.getDouble("minD", -3), prefs.getDouble("maxD", 3), prefs.getDouble("limI1", 11), prefs.getDouble("limI2", 11), prefs.getDouble("limD", 11), prefs.getDouble("density", 11));
        return graph3D;
    }
}

final class Cylindrical_Persistent_3D extends Defaults.Graph3D {

    public static final Graph.Graph3D assignTemplate(Graph.Graph3D graph3D) {
        Preferences prefs = Preferences.userRoot().node("EmeraldMath/graph3D/cylindrical");
        graph3D.setCoordinateSystemParameters(prefs.getDouble("xOrigin", 0), prefs.getDouble("yOrigin", 0), prefs.getDouble("zOrigin", 0), prefs.getDouble("xScale", 1), prefs.getDouble("yScale", 1), prefs.getDouble("zScale", 1), prefs.getDouble("xSpacing", 1), prefs.getDouble("ySpacing", 1), prefs.getDouble("zSpacing", 1));
        graph3D.setPlottingParameters(prefs.getDouble("minI1", 0), prefs.getDouble("maxI1", 2 * Math.PI), prefs.getDouble("minI2", -3), prefs.getDouble("maxI2", 3), prefs.getDouble("minD", -3), prefs.getDouble("maxD", 3), prefs.getDouble("limI1", 11), prefs.getDouble("limI2", 11), prefs.getDouble("limD", 11), prefs.getDouble("density", 11));
        return graph3D;
    }
}

final class Spherical_Persistent_3D extends Defaults.Graph3D {

    public static final Graph.Graph3D assignTemplate(Graph.Graph3D graph3D) {
        Preferences prefs = Preferences.userRoot().node("EmeraldMath/graph3D/spherical");
        graph3D.setCoordinateSystemParameters(prefs.getDouble("xOrigin", 0), prefs.getDouble("yOrigin", 0), prefs.getDouble("zOrigin", 0), prefs.getDouble("xScale", 1), prefs.getDouble("yScale", 1), prefs.getDouble("zScale", 1), prefs.getDouble("xSpacing", 1), prefs.getDouble("ySpacing", 1), prefs.getDouble("zSpacing", 1));
        graph3D.setPlottingParameters(prefs.getDouble("minI1", 0), prefs.getDouble("maxI1", Math.PI), prefs.getDouble("minI2", 0), prefs.getDouble("maxI2", Math.PI / 2), prefs.getDouble("minD", -3), prefs.getDouble("maxD", 3), prefs.getDouble("limI1", 11), prefs.getDouble("limI2", 11), prefs.getDouble("limD", 11), prefs.getDouble("density", 11));
        return graph3D;
    }
}

/* 2D Graphs */
final class Cartesian_Default_2D extends Defaults.Graph2D {

    public static final Graph.Graph2D assignTemplate(Graph.Graph2D graph2D) {
        graph2D.setPlottingParameters(-4.0, 4.0, -4.0, 4.0, 1000.0, 1000.0, 5.0);
        return graph2D;
    }
}

final class Polar_Default_2D extends Defaults.Graph2D {

    public static final Graph.Graph2D assignTemplate(Graph.Graph2D graph2D) {
        graph2D.setPlottingParameters(-2 * Math.PI, 2 * Math.PI, -5.0, 5.0, 1000.0, 1000.0, 50.0);
        return graph2D;
    }
}

final class Polar_Three_To_One_PI_By_Two_2D extends Defaults.Graph2D {

    public static final Graph.Graph2D assignTemplate(Graph.Graph2D graph2D) {
        graph2D.setPlottingParameters(-Math.PI / 2, Math.PI / 2, -5.0, 5.0, 1000.0, 1000.0, 50.0);
        return graph2D;
    }
}

final class Polar_Zero_To_PI_2D extends Defaults.Graph2D {

    public static final Graph.Graph2D assignTemplate(Graph.Graph2D graph2D) {
        graph2D.setPlottingParameters(0.0, Math.PI, -5.0, 5.0, 1000.0, 1000.0, 50.0);
        return graph2D;
    }
}

final class Polar_Zero_To_Two_PI_2D extends Defaults.Graph2D {

    public static final Graph.Graph2D assignTemplate(Graph.Graph2D graph2D) {
        graph2D.setPlottingParameters(0.0, 2 * Math.PI, -5.0, 5.0, 1000.0, 1000.0, 50.0);
        return graph2D;
    }
}


/* 3D Graphs */
final class Cartesian_Default_3D extends Defaults.Graph3D {

    public static final Graph.Graph3D assignTemplate(Graph.Graph3D graph3D) {
        graph3D.setPlottingParameters(-3.0, 3.0, -3.0, 3.0, -3.0, 3.0, 11.0, 11.0, 11.0, 11.0);
        return graph3D;
    }
}

final class Cylindrical_Rho_3D extends Defaults.Graph3D {
    /* I1 = PHI */
    /* I2 = Z */
    /* D = RHO */

    public static final Graph.Graph3D assignTemplate(Graph.Graph3D graph3D) {
        graph3D.setPlottingParameters(0.0, 2 * Math.PI, -3.0, 3.0, -3.0, 3.0, 11.0, 11.0, 11.0, 11.0);
        return graph3D;
    }
}

final class Cylindrical_Phi_3D extends Defaults.Graph3D {
    /* I1 = RHO */
    /* I2 = Z */
    /* D = PHI */

    public static final Graph.Graph3D assignTemplate(Graph.Graph3D graph3D) {
        graph3D.setPlottingParameters(-3.0, 3.0, -3.0, 3.0, 0.0, 2 * Math.PI, 11.0, 11.0, 11.0, 11.0);
        return graph3D;
    }
}

final class Cylindrical_Z_3D extends Defaults.Graph3D {
    /* I1 = RHO */
    /* I2 = PHI */
    /* D = Z */

    public static final Graph.Graph3D assignTemplate(Graph.Graph3D graph3D) {
        graph3D.setPlottingParameters(-3.0, 3.0, 0.0, 2 * Math.PI, -3.0, 3.0, 11.0, 11.0, 11.0, 11.0);
        return graph3D;
    }
}

final class Spherical_R_3D extends Defaults.Graph3D {
    /* I1 = THETA */
    /* I2 = PHI */
    /* D = R */

    public static final Graph.Graph3D assignTemplate(Graph.Graph3D graph3D) {
        graph3D.setPlottingParameters(0.0, Math.PI, 0.0, Math.PI / 2, -3.0, 3.0, 11.0, 11.0, 11.0, 11.0);
        return graph3D;
    }
}

final class Spherical_Theta_3D extends Defaults.Graph3D {
    /* I1 = R */
    /* I2 = PHI */
    /* D = THETA */

    public static final Graph.Graph3D assignTemplate(Graph.Graph3D graph3D) {
        graph3D.setPlottingParameters(-3.0, 3.0, 0.0, Math.PI / 2, 0.0, Math.PI, 11.0, 11.0, 11.0, 11.0);
        return graph3D;
    }
}

final class Spherical_Phi_3D extends Defaults.Graph3D {
    /* I1 = R */
    /* I2 = THETA */
    /* D = PHI */

    public static final Graph.Graph3D assignTemplate(Graph.Graph3D graph3D) {
        graph3D.setPlottingParameters(-3.0, 3.0, 0.0, Math.PI, 0.0, Math.PI / 2, 11.0, 11.0, 11.0, 11.0);
        return graph3D;
    }
}
