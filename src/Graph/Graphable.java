package Graph;

public interface Graphable {   

    public void updateProgressBar();

    public void resetProgressBar();

    public void plot(java.awt.Graphics2D g);

    public void calculatePoints(Graph.PlotType p, boolean uHasAxes, boolean uHasNumbers, boolean uHasMarkers);
}
