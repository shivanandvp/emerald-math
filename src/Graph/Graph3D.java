package Graph;

import Core.Evaluator.ExpressionParseException;
import Core.Function;
import Data.Objects;
import com.sun.j3d.utils.behaviors.vp.OrbitBehavior;
import com.sun.j3d.utils.geometry.GeometryInfo;
import com.sun.j3d.utils.geometry.NormalGenerator;
import com.sun.j3d.utils.universe.SimpleUniverse;
import com.sun.j3d.utils.universe.ViewingPlatform;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.media.j3d.Appearance;
import javax.media.j3d.BoundingSphere;
import javax.media.j3d.BranchGroup;
import javax.media.j3d.Canvas3D;
import javax.media.j3d.DirectionalLight;
import javax.media.j3d.GeometryArray;
import javax.media.j3d.IndexedGeometryArray;
import javax.media.j3d.IndexedQuadArray;
import javax.media.j3d.IndexedTriangleArray;
import javax.media.j3d.PolygonAttributes;
import javax.media.j3d.QuadArray;
import javax.media.j3d.Shape3D;
import javax.vecmath.Color3f;
import javax.vecmath.Point3d;
import javax.vecmath.Point3f;
import javax.vecmath.Vector3f;

public final class Graph3D extends Graph implements Runnable {

    private int numI1, numI2;
    public double minI1, maxI1, minI2, maxI2, minD, maxD, limI1, limI2, limD, density;
    private PlotType p;
    private CoordType coordType;
    private static Thread thread;

    /* Constructors */
    public Graph3D(CoordType uCoordType) {
        graphFunction1 = new Function();
        coordType = uCoordType;
        inputType = InputType.FUNCTION;
        p = PlotType.QUADS;
    }

    public Graph3D(Function uFunction1, CoordType uCoordType) {
        graphFunction1 = uFunction1;
        coordType = uCoordType;
        inputType = InputType.FUNCTION;
        p = PlotType.QUADS;
    }

    public Graph3D(Function uFunction1, Function uFunction2, CoordType uCoordType) {
        graphFunction1 = uFunction1;
        graphFunction2 = uFunction2;
        coordType = uCoordType;
        inputType = InputType.FUNCTION;
        p = PlotType.QUADS;
    }

    /* Private methods */
    private final void calculate() {
        resetProgressBar();
        if (!graphFunction1.hasTwoParameters()) {
            return;
        }
        try {
            graphFunction1.calculatePostfix();
        } catch (ExpressionParseException ex) {
            calculated = true;
            return;
        }

        numI1 = 0;
        numI2 = 0;

        double separation = 1 / density;
        for (double I1 = minI1; I1 <= maxI1; I1 += separation) {
            numI1++;
            for (double I2 = minI2; I2 <= maxI2; I2 += separation) {
                if (numI1 == 1) {
                    numI2++;
                }
            }
        }

        double D = Double.NaN;
        ArrayList<Double> arrayList = new ArrayList<Double>(2);
        arrayList.add(Double.NaN);
        arrayList.add(Double.NaN);
        ArrayList<Point3d> points = new ArrayList<Point3d>();

        switch (coordType) {
            case CARTESIAN_FUNCTION_X:
                for (double I1 = minI1; I1 <= maxI1; I1 += separation) {
                    arrayList.set(0, I1);
                    for (double I2 = minI2; I2 <= maxI2; I2 += separation) {
                        arrayList.set(1, I2);
                        D = graphFunction1.evaluate(arrayList);
                        points.add(new Point3d(D * xScale, I1 * yScale, I2 * zScale));

                        updateProgressBar();
                        if (Objects.stopFlag) {
                            calculated = true;
                            return;
                        }
                    }
                }
                break;
            case CARTESIAN_FUNCTION_Y:
                for (double I1 = minI1; I1 <= maxI1; I1 += separation) {
                    arrayList.set(0, I1);
                    for (double I2 = minI2; I2 <= maxI2; I2 += separation) {
                        arrayList.set(1, I2);
                        D = graphFunction1.evaluate(arrayList);
                        points.add(new Point3d(I1 * xScale, D * yScale, I2 * zScale));

                        updateProgressBar();
                        if (Objects.stopFlag) {
                            calculated = true;
                            return;
                        }
                    }
                }
                break;
            case CARTESIAN_FUNCTION_Z:
                for (double I1 = minI1; I1 <= maxI1; I1 += separation) {
                    arrayList.set(0, I1);
                    for (double I2 = minI2; I2 <= maxI2; I2 += separation) {
                        arrayList.set(1, I2);
                        D = graphFunction1.evaluate(arrayList);
                        points.add(new Point3d(I1 * xScale, I2 * yScale, D * zScale));

                        updateProgressBar();
                        if (Objects.stopFlag) {
                            calculated = true;
                            return;
                        }
                    }
                }
                break;

            case CYLINDRICAL_FUNCTION_RHO:
                for (double I1 = minI1; I1 <= maxI1; I1 += separation) {
                    arrayList.set(0, I1);
                    for (double I2 = minI2; I2 <= maxI2; I2 += separation) {
                        arrayList.set(1, I2);
                        D = graphFunction1.evaluate(arrayList);
                        points.add(new Point3d(D * Math.cos(I1) * xScale, D * Math.sin(I1) * yScale, I2 * zScale));

                        updateProgressBar();
                        if (Objects.stopFlag) {
                            calculated = true;
                            return;
                        }
                    }
                }
                break;
            case CYLINDRICAL_FUNCTION_PHI:
                for (double I1 = minI1; I1 <= maxI1; I1 += separation) {
                    arrayList.set(0, I1);
                    for (double I2 = minI2; I2 <= maxI2; I2 += separation) {
                        arrayList.set(1, I2);
                        D = graphFunction1.evaluate(arrayList);
                        points.add(new Point3d(I1 * Math.cos(D) * xScale, I1 * Math.sin(D) * yScale, I2 * zScale));

                        updateProgressBar();
                        if (Objects.stopFlag) {
                            calculated = true;
                            return;
                        }
                    }
                }
                break;
            case CYLINDRICAL_FUNCTION_Z:
                for (double I1 = minI1; I1 <= maxI1; I1 += separation) {
                    arrayList.set(0, I1);
                    for (double I2 = minI2; I2 <= maxI2; I2 += separation) {
                        arrayList.set(1, I2);
                        D = graphFunction1.evaluate(arrayList);
                        points.add(new Point3d(I1 * Math.cos(I2) * xScale, I1 * Math.sin(I2) * yScale, D * zScale));

                        updateProgressBar();
                        if (Objects.stopFlag) {
                            calculated = true;
                            return;
                        }
                    }
                }
                break;
            case SPHERICAL_FUNCTION_R:
                for (double I1 = minI1; I1 <= maxI1; I1 += separation) {
                    arrayList.set(0, I1);
                    for (double I2 = minI2; I2 <= maxI2; I2 += separation) {
                        arrayList.set(1, I2);
                        D = graphFunction1.evaluate(arrayList);
                        points.add(new Point3d(D * Math.sin(I1) * Math.cos(I2) * xScale, D * Math.sin(I1) * Math.sin(I2) * yScale, D * Math.cos(I1) * zScale));

                        updateProgressBar();
                        if (Objects.stopFlag) {
                            calculated = true;
                            return;
                        }
                    }
                }
                break;
            case SPHERICAL_FUNCTION_THETA:
                for (double I1 = minI1; I1 <= maxI1; I1 += separation) {
                    arrayList.set(0, I1);
                    for (double I2 = minI2; I2 <= maxI2; I2 += separation) {
                        arrayList.set(1, I2);
                        D = graphFunction1.evaluate(arrayList);
                        points.add(new Point3d(I1 * Math.sin(D) * Math.cos(I2) * xScale, I1 * Math.sin(D) * Math.sin(I2) * yScale, I1 * Math.cos(D) * zScale));

                        updateProgressBar();
                        if (Objects.stopFlag) {
                            calculated = true;
                            return;
                        }
                    }
                }
                break;
            case SPHERICAL_FUNCTION_PHI:
                for (double I1 = minI1; I1 <= maxI1; I1 += separation) {
                    arrayList.set(0, I1);
                    for (double I2 = minI2; I2 <= maxI2; I2 += separation) {
                        arrayList.set(1, I2);
                        D = graphFunction1.evaluate(arrayList);
                        points.add(new Point3d(I1 * Math.sin(I2) * Math.cos(D) * xScale, I1 * Math.sin(I2) * Math.sin(D) * yScale, I1 * Math.cos(I2) * zScale));

                        updateProgressBar();
                        if (Objects.stopFlag) {
                            calculated = true;
                            return;
                        }
                    }
                }
                break;
        }

        // Create shapes  

        parentGraphInternalFrame.getCanvasPanel().removeAll();
        Canvas3D canvas3D = new Canvas3D(SimpleUniverse.getPreferredConfiguration());
        canvas3D.setSize(new Dimension(700, 700));
        parentGraphInternalFrame.getCanvasPanel().add(canvas3D, "CENTER");

        int indices[], count = 0;
        GeometryInfo geometryInfo = null;
        NormalGenerator normalGenerator = null;
        IndexedGeometryArray indexedGeometryArray = null;
        switch (p) {
            case QUADS:
                indices = new int[(numI1 - 1) * (numI2 - 1) * 4];
                indexedGeometryArray = new IndexedQuadArray(points.size(), GeometryArray.COORDINATES | GeometryArray.NORMALS, (numI1 - 1) * (numI2 - 1) * 4);
                for (int r = 0; r < (numI1 - 1); r++) {
                    for (int c = 0; c < (numI2 - 1); c++) {
                        indices[count++] = r * numI2 + c;
                        indices[count++] = (r + 1) * numI2 + c;
                        indices[count++] = (r + 1) * numI2 + c + 1;
                        indices[count++] = r * numI2 + c + 1;
                    }
                }
                indexedGeometryArray.setCoordinates(0, points.toArray(new Point3d[1]));
                indexedGeometryArray.setCoordinateIndices(0, indices);
                geometryInfo = new GeometryInfo(indexedGeometryArray);
                geometryInfo.recomputeIndices();
                normalGenerator = new NormalGenerator();
                normalGenerator.generateNormals(geometryInfo);
                break;
            case QUAD_WIREFRAME:
                indices = new int[(numI1 - 1) * (numI2 - 1) * 4];
                indexedGeometryArray = new IndexedQuadArray(points.size(), GeometryArray.COORDINATES | GeometryArray.NORMALS, (numI1 - 1) * (numI2 - 1) * 4);
                for (int r = 0; r < (numI1 - 1); r++) {
                    for (int c = 0; c < (numI2 - 1); c++) {
                        indices[count++] = r * numI2 + c;
                        indices[count++] = (r + 1) * numI2 + c;
                        indices[count++] = (r + 1) * numI2 + c + 1;
                        indices[count++] = r * numI2 + c + 1;
                    }
                }
                indexedGeometryArray.setCoordinates(0, points.toArray(new Point3d[1]));
                indexedGeometryArray.setCoordinateIndices(0, indices);
                geometryInfo = new GeometryInfo(indexedGeometryArray);
                normalGenerator = new NormalGenerator();
                normalGenerator.generateNormals(geometryInfo);
                break;
            case TRIANGLES:
                indices = new int[(numI1 - 1) * (numI2 - 1) * 6];
                indexedGeometryArray = new IndexedTriangleArray(points.size(), GeometryArray.COORDINATES | GeometryArray.NORMALS, (numI1 - 1) * (numI2 - 1) * 6);
                for (int r = 0; r < (numI1 - 1); r++) {
                    for (int c = 0; c < (numI2 - 1); c++) {
                        indices[count++] = r * numI2 + c;
                        indices[count++] = (r + 1) * numI2 + c;
                        indices[count++] = (r + 1) * numI2 + c + 1;
                        indices[count++] = (r + 1) * numI2 + c + 1;
                        indices[count++] = r * numI2 + c + 1;
                        indices[count++] = r * numI2 + c;
                    }
                }
                indexedGeometryArray.setCoordinates(0, points.toArray(new Point3d[1]));
                indexedGeometryArray.setCoordinateIndices(0, indices);
                geometryInfo = new GeometryInfo(indexedGeometryArray);
                normalGenerator = new NormalGenerator();
                normalGenerator.generateNormals(geometryInfo);
                break;
            case TRIANGLE_WIREFRAME:
                indices = new int[(numI1 - 1) * (numI2 - 1) * 6];
                indexedGeometryArray = new IndexedTriangleArray(points.size(), GeometryArray.COORDINATES | GeometryArray.NORMALS, (numI1 - 1) * (numI2 - 1) * 6);
                for (int r = 0; r < (numI1 - 1); r++) {
                    for (int c = 0; c < (numI2 - 1); c++) {
                        indices[count++] = r * numI2 + c;
                        indices[count++] = (r + 1) * numI2 + c;
                        indices[count++] = (r + 1) * numI2 + c + 1;
                        indices[count++] = (r + 1) * numI2 + c + 1;
                        indices[count++] = r * numI2 + c + 1;
                        indices[count++] = r * numI2 + c;
                    }
                }
                indexedGeometryArray.setCoordinates(0, points.toArray(new Point3d[1]));
                indexedGeometryArray.setCoordinateIndices(0, indices);
                geometryInfo = new GeometryInfo(indexedGeometryArray);
                normalGenerator = new NormalGenerator();
                normalGenerator.generateNormals(geometryInfo);
                break;
            default:
                geometryInfo = new GeometryInfo(new QuadArray(1, QuadArray.COORDINATES));
                break;
        }
        BranchGroup objRoot = new BranchGroup();
        objRoot.setCapability(BranchGroup.ALLOW_DETACH);

        // TODO : Customize lighting support for 3D graphs
        Color3f lightColor1 = new Color3f(0.1f, 0.9f, 0.1f);
        Vector3f lightDirection1 = new Vector3f(2.0f, 2.0f, -11.0f);
        Point3f location1 = new Point3f(0, 0, (float) (2 * Math.sqrt(2 / 3)));
        Point3f attenuation1 = new Point3f(3.0f, 3.0f, 3.0f);
        float spreadAngle1 = 3.0f, concentration1 = 1.0f;
        DirectionalLight directionalLight1 = new DirectionalLight(true, lightColor1, lightDirection1);
        directionalLight1.setInfluencingBounds(new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 500));
        objRoot.addChild(directionalLight1);

        Appearance appearance = new Appearance();
        appearance.setCapability(Appearance.ALLOW_POLYGON_ATTRIBUTES_WRITE);
        appearance.setMaterial(Data.Objects.material);

        if (p == PlotType.QUADS || p == PlotType.TRIANGLES) {
            appearance.setPolygonAttributes(new PolygonAttributes(PolygonAttributes.POLYGON_FILL, PolygonAttributes.CULL_NONE, 0.01f, false));
        } else if (p == PlotType.QUAD_WIREFRAME || p == PlotType.TRIANGLE_WIREFRAME) {
            appearance.setPolygonAttributes(new PolygonAttributes(PolygonAttributes.POLYGON_LINE, PolygonAttributes.CULL_NONE, 0.01f, false));
        }

        Shape3D shape3D = new Shape3D(geometryInfo.getGeometryArray(), appearance);
        objRoot.addChild(shape3D);

        drawGrid();

        BranchGroup branchGroup = new BranchGroup();
        branchGroup.setCapability(BranchGroup.ALLOW_CHILDREN_WRITE);
        branchGroup.setCapability(BranchGroup.ALLOW_CHILDREN_EXTEND);
        branchGroup.setCapability(BranchGroup.ALLOW_DETACH);
        branchGroup.addChild(objRoot);
        SimpleUniverse u = new SimpleUniverse(canvas3D);
        if (!branchGroup.isCompiled()) {
            branchGroup.compile();
            u.addBranchGraph(branchGroup);
        }

        OrbitBehavior orbit = new OrbitBehavior(canvas3D, OrbitBehavior.REVERSE_ALL | OrbitBehavior.STOP_ZOOM);
        orbit.setSchedulingBounds(new BoundingSphere(new Point3d(0, 0, 0), 100));
        ViewingPlatform viewingPlatform = u.getViewingPlatform();
        viewingPlatform.setNominalViewingTransform();
        viewingPlatform.setViewPlatformBehavior(orbit);

        drawGrid();
        calculated = true;
    }

    private final void drawGrid() {
        // TODO : Draw grid, numbers, markers for 3D cartesian graphs.
        if (hasAxes) {
            if (coordType == CoordType.CARTESIAN_FUNCTION_X || coordType == CoordType.CARTESIAN_FUNCTION_Y || coordType == CoordType.CARTESIAN_FUNCTION_Z) {
                ////BranchGroup axesRoot = new BranchGroup();
                ////axesRoot.addChild(new Cylinder(0.05f, , numI1, numI1, numI1, null));
                ////axesRoot.compile();
                ////branchGroup.addChild(axesRoot);
            }
        }
    }

    /* Get methods */
    public final PlotType getPlotType() {
        return p;
    }

    public final CoordType getCoordType() {
        return coordType;
    }

    public final Point3d getLocation(double uI1, double uI2, double uD) {
        return new Point3d(xOrigin + uI1 * xScale, yOrigin + uI2 * yScale, zOrigin + uD * zScale);
    }

    public final Point3d getLocation(Point3d uPoint) {
        return new Point3d(xOrigin + uPoint.x * xScale, yOrigin + uPoint.y * yScale, zOrigin + uPoint.z * zScale);
    }

    /* Set methods */
    public final void setPlotType(PlotType uPlotType) {
        p = uPlotType;
    }

    public final void setCoordType(CoordType uCoordType) {
        coordType = uCoordType;
    }

    public final void setPlottingParameters(double uMinI1, double uMaxI1, double uMinI2, double uMaxI2, double uMinD, double uMaxD, double uLimI1, double uLimI2, double uLimD, double uDensity) {
        minI1 = uMinI1;
        maxI1 = uMaxI1;
        minI2 = uMinI2;
        maxI2 = uMaxI2;
        minD = uMinD;
        maxD = uMaxD;
        limI1 = uLimI1;
        limI2 = uLimI2;
        limD = uLimD;
        density = uDensity;
    }

    public final void setCoordinateSystemParameters(double uXOrigin, double uYOrigin, double uZOrigin, double uXScale, double uYScale, double uZScale, double uXSpacing, double uYSpacing, double uZSpacing) {
        xOrigin = uXOrigin;
        yOrigin = uYOrigin;
        zOrigin = uZOrigin;
        xScale = uXScale;
        yScale = uYScale;
        zScale = uZScale;
        gridXSpacing = uXSpacing;
        gridYSpacing = uXSpacing;
        gridZSpacing = uZSpacing;
    }

    @Override
    public final void updateProgressBar() {
        Objects.progressBar.setValue(++progressBarState);
    }

    @Override
    public final void resetProgressBar() {
        Objects.progressBar.setMinimum(0);
        Objects.progressBar.setMaximum((int) ((maxI1 - minI1) * density * (maxI2 - minI2) * density));
        progressBarState = 0;
    }

    @Override
    public final void calculatePoints(PlotType uP, boolean uHasAxes, boolean uHasNumbers, boolean uHasMarkers) {
        if (!calculated) {
            return;
        }
        calculated = false;
        hasAxes = uHasAxes;
        hasNumbers = uHasNumbers;
        hasMarkers = uHasMarkers;
        p = uP;
        if (thread == null || !thread.isAlive()) {
            thread = new Thread(this);
            thread.start();
        } else if (thread.isAlive()) {
            try {
                thread.join();
                thread = new Thread(this);
                thread.start();
            } catch (InterruptedException ex) {
                Logger.getLogger(Graph3D.class.getName()).log(Level.SEVERE, "InterruptedException in method calculatePoints", ex);
            }
        }
    }

    @Override
    public final void plot(Graphics2D g) {
        // TODO : Customize Graphics2D properties for Canvas Panel
    }

    /* Implemented methods */
    @Override
    public final void run() {
        calculate();

        try {
            parentGraphInternalFrame.setMaximum(true);
            parentGraphInternalFrame.show();
        } catch (PropertyVetoException ex) {
        }
    }

    /* Enumerations */
    public enum CoordType {

        CARTESIAN_FUNCTION_X, CARTESIAN_FUNCTION_Y, CARTESIAN_FUNCTION_Z,
        CYLINDRICAL_FUNCTION_RHO, CYLINDRICAL_FUNCTION_PHI, CYLINDRICAL_FUNCTION_Z,
        SPHERICAL_FUNCTION_R, SPHERICAL_FUNCTION_THETA, SPHERICAL_FUNCTION_PHI,
        CARTESIAN_PARAMETRIC_FUNCTION_CURVE
    }
}
