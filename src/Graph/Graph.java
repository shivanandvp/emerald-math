package Graph;

import Core.*;
import UI.Custom.GraphInternalFrame;
import java.awt.Graphics2D;

public abstract class Graph implements Graphable {

    protected boolean calculated = true;
    protected boolean hasAxes = true, hasNumbers = true, hasMarkers = true;
    protected int progressBarState;
    public double xOrigin, yOrigin, zOrigin, xScale, yScale, zScale, gridXSpacing, gridYSpacing, gridZSpacing;
    protected Graph.InputType inputType;
    protected Function graphFunction1, graphFunction2;
    public GraphInternalFrame parentGraphInternalFrame;

    /* Get methods */
    public final Function getFunction1() {
        return graphFunction1;
    }

    public final Function getFunction2() {
        return graphFunction2;
    }

    public final InputType getInputType() {
        return inputType;
    }

    public final double getXOrigin() {
        return xOrigin;
    }

    public final double getYOrigin() {
        return yOrigin;
    }

    public final double getZOrigin() {
        return zOrigin;
    }

    public final double getXScale() {
        return xScale;
    }

    public final double getYScale() {
        return yScale;
    }

    public final double getZScale() {
        return zScale;
    }

    /* Set methods */
    public final void setFunction1(Function uGraphFunction1) {
        graphFunction1 = uGraphFunction1;
    }

    public final void setFunction2(Function uGraphFunction2) {
        graphFunction2 = uGraphFunction2;
    }

    public final void setFunction3(Function uGraphFunction3) {
        graphFunction2 = uGraphFunction3;
    }

    public final void setInputType(InputType uInputType) {
        inputType = uInputType;
    }

    /* Interface Methods */
    @Override
    public abstract void updateProgressBar();

    @Override
    public abstract void resetProgressBar();

    @Override
    public abstract void plot(Graphics2D g);

    @Override
    public abstract void calculatePoints(PlotType p, boolean uHasAxes, boolean uHasNumbers, boolean uHasMarkers);

    /* Enumerations */
    public enum PlotType {

        LINES, POINTS, QUADS, TRIANGLES, QUAD_WIREFRAME, TRIANGLE_WIREFRAME
    }

    public enum InputType {

        FUNCTION, PARAMETRIC
    }
}
