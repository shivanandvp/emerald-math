package Graph;

import Core.*;
import Data.Objects;
import Core.Evaluator.ExpressionParseException;
import Data.Defaults;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Shape;
import java.awt.geom.Line2D;
import java.awt.geom.Path2D;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.beans.PropertyVetoException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Graph2D extends Graph implements Runnable {

    private int pointsCount = 0;
    public double minI, maxI, minD, maxD, limI, limD, density;
    private double prevX = 0, prevY = 0, deltaX = 0, deltaY = 0;
    private PlotType p;
    private CoordType coordType;
    private Point2D.Double point;
    private Path2D.Double path2D;
    private static Thread thread;
    private ArrayList<Shape> primitiveShapes;

    /* Constructors */
    public Graph2D() {
        graphFunction1 = new Function();
        graphFunction2 = null;
        primitiveShapes = new ArrayList<Shape>();
        coordType = CoordType.CARTESIAN;
        inputType = InputType.FUNCTION;
    }

    public Graph2D(Function uGraphFunction1) {
        graphFunction1 = uGraphFunction1;
        graphFunction2 = null;
        primitiveShapes = new ArrayList<Shape>();
        coordType = CoordType.CARTESIAN;
        inputType = InputType.FUNCTION;
    }

    public Graph2D(Function uGraphFunction1, Function uGraphFunction2) {
        graphFunction1 = uGraphFunction1;
        graphFunction2 = uGraphFunction2;
        primitiveShapes = new ArrayList<Shape>();
        coordType = CoordType.CARTESIAN;
        inputType = InputType.PARAMETRIC;
    }

    public Graph2D(Function uGraphFunction1, CoordType uCoordType) {
        graphFunction1 = uGraphFunction1;
        graphFunction2 = null;
        primitiveShapes = new ArrayList<Shape>();
        coordType = uCoordType;
        inputType = InputType.FUNCTION;
    }

    public Graph2D(Function uGraphFunction1, Function uGraphFunction2, CoordType uCoordType) {
        graphFunction1 = uGraphFunction1;
        graphFunction2 = uGraphFunction2;
        primitiveShapes = new ArrayList<Shape>();
        coordType = uCoordType;
        inputType = InputType.PARAMETRIC;
    }

    /* Private methods */
    private final void incrementalPlotLINES(double pointX, double pointY) {
        if (Objects.stopFlag) {
            calculated = true;
            return;
        }
        point = getLocation(pointX, pointY);
        deltaX = Math.abs(point.x - prevX);
        deltaY = Math.abs(point.y - prevY);
        if (deltaX < limI && deltaY < limD && (deltaX > Defaults.Graph2D.minSeparation || deltaY > Defaults.Graph2D.minSeparation)) {
            path2D.lineTo(point.x, point.y);
            prevX = point.x;
            prevY = point.y;
        } else if (deltaX >= limI || deltaY >= limD) {
            path2D.moveTo(point.x, point.y);
            prevX = point.x;
            prevY = point.y;
        } else {
            //// TODO: Log if delta is negative
        }
        updateProgressBar();
    }

    private final void incrementalPlotPOINTS(double pointX, double pointY) {
        if (Objects.stopFlag) {
            calculated = true;
            return;
        }
        primitiveShapes.add(new Rectangle2D.Double(getLocation(pointX, pointY).x - Defaults.Graph2D.pointSize / 2, getLocation(pointX, pointY).y + Defaults.Graph2D.pointSize / 2, Defaults.Graph2D.pointSize, Defaults.Graph2D.pointSize));
        updateProgressBar();
    }

    private final void calculate() {
        resetProgressBar();

        if (!graphFunction1.hasSingleParameter()) {
            calculated = true;
            return;
        }
        if (inputType == Graph.InputType.PARAMETRIC && (graphFunction2 == null || !graphFunction2.hasSingleParameter())) {
            calculated = true;
            return;
        }

        pointsCount = 0;
        prevX = 0;
        prevY = 0;
        deltaX = 0;
        deltaY = 0;

        double D = Double.NaN;
        double pointX = Double.NaN, pointY = Double.NaN;
        double separation = 1 / density;
        ArrayList<Double> arrayList = new ArrayList<Double>(1);
        arrayList.add(Double.NaN);
        primitiveShapes = new ArrayList<Shape>();

        for (double I = minI; I <= maxI; I += separation) {
            ++pointsCount;
        }

        if (p == PlotType.LINES) {
            path2D = new Path2D.Double(Path2D.WIND_NON_ZERO, pointsCount);
            path2D.moveTo(Double.NaN, Double.NaN);
            if (inputType == Graph.InputType.FUNCTION) {
                try {
                    graphFunction1.calculatePostfix();
                } catch (ExpressionParseException ex) {
                    calculated = true;
                    return;
                }
                switch (coordType) {
                    case CARTESIAN:
                        for (double I = minI; I <= maxI; I += separation) {
                            arrayList.set(0, I);
                            D = graphFunction1.evaluate(arrayList);
                            pointX = I;
                            pointY = D;
                            incrementalPlotLINES(pointX, pointY);
                        }
                        break;
                    case POLAR:
                        for (double I = minI; I <= maxI; I += separation) {
                            arrayList.set(0, I);
                            double r = graphFunction1.evaluate(arrayList);
                            pointX = r * Math.cos(I);
                            pointY = r * Math.sin(I);
                            incrementalPlotLINES(pointX, pointY);
                        }
                        break;
                }
            } else if (inputType == Graph.InputType.PARAMETRIC) {
                try {
                    graphFunction1.calculatePostfix();
                    graphFunction2.calculatePostfix();
                } catch (ExpressionParseException expressionParseException) {
                    calculated = true;
                    return;
                }
                switch (coordType) {
                    case CARTESIAN:
                        for (double I = minI; I <= maxI; I += separation) {
                            arrayList.set(0, I);
                            pointX = graphFunction1.evaluate(arrayList);
                            pointY = graphFunction2.evaluate(arrayList);
                            incrementalPlotLINES(pointX, pointY);
                        }
                        break;
                    case POLAR:
                        double r,
                         theta;
                        for (double I = minI; I <= maxI; I += separation) {
                            arrayList.set(0, I);
                            r = graphFunction1.evaluate(arrayList);
                            theta = graphFunction2.evaluate(arrayList);
                            pointX = r * Math.cos(theta);
                            pointY = r * Math.sin(theta);
                            incrementalPlotLINES(pointX, pointY);
                        }
                        break;
                }
            }
            primitiveShapes.add(path2D);
        } else if (p == PlotType.POINTS) {
            if (inputType == Graph.InputType.FUNCTION) {
                try {
                    graphFunction1.calculatePostfix();
                } catch (ExpressionParseException ex) {
                    calculated = true;
                    return;
                }
                switch (coordType) {
                    case CARTESIAN:
                        for (double I = minI; I <= maxI; I += separation) {
                            arrayList.set(0, I);
                            D = graphFunction1.evaluate(arrayList);
                            pointX = I;
                            pointY = D;
                            incrementalPlotPOINTS(pointX, pointY);
                        }
                        break;
                    case POLAR:
                        for (double I = minI; I <= maxI; I += separation) {
                            arrayList.set(0, I);
                            double r = graphFunction1.evaluate(arrayList);
                            pointX = r * Math.cos(I);
                            pointY = r * Math.sin(I);
                            incrementalPlotPOINTS(pointX, pointY);
                        }
                        break;
                }
            } else if (inputType == Graph.InputType.PARAMETRIC) {
                try {
                    graphFunction1.calculatePostfix();
                    graphFunction2.calculatePostfix();
                } catch (ExpressionParseException expressionParseException) {
                    calculated = true;
                    return;
                }
                switch (coordType) {
                    case CARTESIAN:
                        for (double I = minI; I <= maxI; I += separation) {
                            arrayList.set(0, I);
                            pointX = graphFunction1.evaluate(arrayList);
                            pointY = graphFunction2.evaluate(arrayList);
                            incrementalPlotPOINTS(pointX, pointY);
                        }
                        break;
                    case POLAR:
                        double r,
                         theta;
                        for (double I = minI; I <= maxI; I += separation) {
                            arrayList.set(0, I);
                            r = graphFunction1.evaluate(arrayList);
                            theta = graphFunction2.evaluate(arrayList);
                            pointX = r * Math.cos(theta);
                            pointY = r * Math.sin(theta);
                            incrementalPlotPOINTS(pointX, pointY);
                        }
                        break;
                }
            }
        }

        drawGrid();
        parentGraphInternalFrame.repaint();
        calculated = true;
        return;
    }

    private final void drawGrid() {
        if (hasAxes) {
            primitiveShapes.add(new Line2D.Double(getLocation(minI - 1, 0), getLocation(maxI + 1, 0)));
            primitiveShapes.add(new Line2D.Double(getLocation(0, minD - 1), getLocation(0, maxD + 1)));
        }
        if (hasNumbers) {
            // TODO : Add numbers for a 2D graph grid.
        }
        if (hasMarkers) {
            double xStart = minI * xScale + xOrigin, xEnd = maxI * xScale + xOrigin, xSpacing = gridXSpacing * xScale;
            for (double i = xOrigin; i >= xStart; i -= xSpacing) {
                primitiveShapes.add(new Line2D.Double(i, yOrigin - Defaults.Graph2D.markerWidth / 2, i, yOrigin + Defaults.Graph2D.markerWidth / 2));
            }
            for (double i = xOrigin; i <= xEnd; i += xSpacing) {
                primitiveShapes.add(new Line2D.Double(i, yOrigin - Defaults.Graph2D.markerWidth / 2, i, yOrigin + Defaults.Graph2D.markerWidth / 2));
            }
            double yStart = -minD * yScale + yOrigin, yEnd = -maxD * yScale + yOrigin, ySpacing = -gridYSpacing * yScale;
            for (double i = yOrigin; i <= yStart; i -= ySpacing) {
                primitiveShapes.add(new Line2D.Double(xOrigin - Defaults.Graph2D.markerWidth / 2, i, xOrigin + Defaults.Graph2D.markerWidth / 2, i));
            }
            for (double i = yOrigin; i >= yEnd; i += ySpacing) {
                primitiveShapes.add(new Line2D.Double(xOrigin - Defaults.Graph2D.markerWidth / 2, i, xOrigin + Defaults.Graph2D.markerWidth / 2, i));
            }
        }
    }

    /* Get methods */
    public final PlotType getPlotType() {
        return p;
    }

    public final CoordType getCoordType() {
        return coordType;
    }

    public final Point2D.Double getLocation(double uI, double uD) {
        return new Point2D.Double(xOrigin + uI * xScale, yOrigin - uD * yScale);
    }

    public final Point2D.Double getLocation(Point2D.Double uPoint) {
        return new Point2D.Double(xOrigin + uPoint.x * xScale, yOrigin - uPoint.y * yScale);
    }

    /* Set methods */
    public final void setPlotType(PlotType uPlotType) {
        p = uPlotType;
    }

    public final void setCoordType(CoordType uCoordType) {
        coordType = uCoordType;
    }

    public final void setPlottingParameters(double uMinI, double uMaxI, double uMinD, double uMaxD, double uLimI, double uLimD, double uDensity) {
        minI = uMinI;
        maxI = uMaxI;
        minD = uMinD;
        maxD = uMaxD;
        limI = uLimI;
        limD = uLimD;
        density = uDensity;
    }

    public final void setCoordinateSystemParameters(double uXOrigin, double uYOrigin, double uXScale, double uYScale, double uXSpacing, double uYSpacing) {
        xOrigin = uXOrigin;
        yOrigin = uYOrigin;
        xScale = uXScale;
        yScale = uYScale;
        gridXSpacing = uXSpacing;
        gridYSpacing = uYSpacing;
    }

    @Override
    public final void updateProgressBar() {
        Objects.progressBar.setValue(++progressBarState);
    }

    @Override
    public final void resetProgressBar() {
        Objects.progressBar.setMinimum(0);
        Objects.progressBar.setMaximum((int) ((maxI - minI) * density));
        progressBarState = 0;
    }

    @Override
    public final void calculatePoints(PlotType uP, boolean uHasAxes, boolean uHasNumbers, boolean uHasMarkers) {
        if (!calculated) {
            return;
        }
        calculated = false;
        hasAxes = uHasAxes;
        hasNumbers = uHasNumbers;
        hasMarkers = uHasMarkers;
        p = uP;
        if (thread == null || !thread.isAlive()) {
            thread = new Thread(this);
            thread.start();
        } else if (thread.isAlive()) {
            try {
                thread.join();
                thread = new Thread(this);
                thread.start();
            } catch (InterruptedException ex) {
                Logger.getLogger(Graph2D.class.getName()).log(Level.SEVERE, "InterruptedException in method calculatePoints", ex);
            }
        }
    }

    @Override
    public final void plot(Graphics2D g) {
        if (calculated) {
            g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            for (Shape shape : primitiveShapes) {
                g.draw(shape);
            }
        }
    }

    /* Implemented methods */
    @Override
    public final void run() {
        calculate();

        try {
            parentGraphInternalFrame.setMaximum(true);
            parentGraphInternalFrame.show();
        } catch (PropertyVetoException ex) {
        }
    }

    /* Enumerations */
    public enum CoordType {

        CARTESIAN, POLAR
    }
}
