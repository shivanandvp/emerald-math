package UI;

public class AboutFrame extends javax.swing.JInternalFrame {

    public AboutFrame() {
        initComponents();
        jEditorPane1.setCaretPosition(0);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();

        setClosable(true);
        setTitle("About Emerald Math");

        jEditorPane1.setEditable(false);
        jEditorPane1.setFont(new java.awt.Font("Georgia", 0, 14)); // NOI18N
        jEditorPane1.setText("Programmer :\nShivanand Pattanshetti ( shivanand.pattanshetti@gmail.com )\n\nWebsite :\nwww.chromehub.webs.com\n\nDownload Location : \nwww.chromehub.webs.com\\apps\\links\n\nVersion : \n04042010 BETA\nWarning : This is a BETA version.\n\nLast Modified on : \n04.04.2010\n07:00 P.M.\n\nLatest Modifications : \n1. Added toolbar with icons.\n2. Added grapher.\n3. Added calculator.\n\n\n\n");
        jScrollPane1.setViewportView(jEditorPane1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 376, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
