package UI.Dialog;

import java.util.ArrayList;
import javax.swing.text.JTextComponent;

public class FunctionChooser extends TextReplacerFrame {

    private static ArrayList<String> functionNames;
    private static ArrayList<Integer> noOfVariables;
    private static ArrayList<String> descriptions;

    public FunctionChooser(JTextComponent jTextComponent) {
        super(jTextComponent);
        initComponents();
        functionNames = new ArrayList<String>(35);
        noOfVariables = new ArrayList<Integer>(35);
        descriptions = new ArrayList<String>(35);

        functionNames.add("pow");
        noOfVariables.add(2);
        descriptions.add("Expression1 ^ Expression2");

        functionNames.add("sqrt");
        noOfVariables.add(1);
        descriptions.add("Square root of the expression.");

        functionNames.add("cbrt");
        noOfVariables.add(1);
        descriptions.add("Cube root of the expression.");

        functionNames.add("sin");
        noOfVariables.add(1);
        descriptions.add("Sine of the expression.");

        functionNames.add("cos");
        noOfVariables.add(1);
        descriptions.add("Cos of the expression.");

        functionNames.add("tan");
        noOfVariables.add(1);
        descriptions.add("Tan of the expression.");

        functionNames.add("sinh");
        noOfVariables.add(1);
        descriptions.add("Hyperbolic Sine of the expression.");

        functionNames.add("cosh");
        noOfVariables.add(1);
        descriptions.add("Hyperbolic Cos of the expression.");

        functionNames.add("tanh");
        noOfVariables.add(1);
        descriptions.add("Hyperbolic Tan of the expression.");

        functionNames.add("ln");
        noOfVariables.add(1);
        descriptions.add("Natural logarithm of the expression.");

        functionNames.add("exp");
        noOfVariables.add(1);
        descriptions.add("E ^ Expression.");

        functionNames.add("log");
        noOfVariables.add(1);
        descriptions.add("Base 10 logarithm of the expression.");

        functionNames.add("reci");
        noOfVariables.add(1);
        descriptions.add("Reciprocal of the expression.");

        functionNames.add("hypot");
        noOfVariables.add(2);
        descriptions.add("Expression1 ^ 2 + Expression2 ^ 2.");

        functionNames.add("asin");
        noOfVariables.add(1);
        descriptions.add("Arc Sine of the expression.");

        functionNames.add("acos");
        noOfVariables.add(1);
        descriptions.add("Arc Cos of the expression.");

        functionNames.add("atan");
        noOfVariables.add(1);
        descriptions.add("Arc Tan of the expression.");

        functionNames.add("asinh");
        noOfVariables.add(1);
        descriptions.add("Arc Hyperbolic Sine of the expression.");

        functionNames.add("acosh");
        noOfVariables.add(1);
        descriptions.add("Arc Hyperbolic Cos of the expression.");

        functionNames.add("atanh");
        noOfVariables.add(1);
        descriptions.add("Arc Hyperbolic Tan of the expression.");

        functionNames.add("atan2");
        noOfVariables.add(2);
        descriptions.add("Arc Tan of Expression1 / Expression2.");

        functionNames.add("abs");
        noOfVariables.add(1);
        descriptions.add("Absolute value of the expression.");

        functionNames.add("ceil");
        noOfVariables.add(1);
        descriptions.add("Greatest integer of the expression.");

        functionNames.add("floor");
        noOfVariables.add(1);
        descriptions.add("Smallest integer of the expression.");

        functionNames.add("round");
        noOfVariables.add(1);
        descriptions.add("Rounded value of the expression.");

        functionNames.add("factorial");
        noOfVariables.add(1);
        descriptions.add("Factorial of the expression.");

        functionNames.add("nCr");
        noOfVariables.add(2);
        descriptions.add("nCr where n = Expression1 and r = Expression2.");

        functionNames.add("nPr");
        noOfVariables.add(2);
        descriptions.add("nPr where n = Expression1 and r = Expression2.");

        functionNames.add("random");
        noOfVariables.add(2);
        descriptions.add("Random value between Expression1 and Expression2.");

        functionNames.add("max");
        noOfVariables.add(2);
        descriptions.add("Maximum of all expressions.");

        functionNames.add("min");
        noOfVariables.add(2);
        descriptions.add("Minimum of all expressions.");

        functionNames.add("sum");
        noOfVariables.add(4);
        descriptions.add("Summation of Expression4 when variable Expression1(which is variable name) increments from Expression2 to Expression3.");

        functionNames.add("product");
        noOfVariables.add(4);
        descriptions.add("Product of Expression4 when variable Expression1(which is variable name) increments from Expression2 to Expression3.");

        functionNames.add("repeat");
        noOfVariables.add(4);
        descriptions.add("Recursing of Expression4 when variable Expression1(which is variable name) starts from Expression2 repeated Expression3 times.");

        functionNames.add("determinant");
        noOfVariables.add(4);
        descriptions.add("Determinant of all expressions in row major form.");

        for (int i = 0; i < functionNames.size(); i++) {
            jComboBox1.addItem(functionNames.get(i));
        }
        jComboBox1.setSelectedIndex(0);
    }

    private final void updateSelection() {
        int index = jComboBox1.getSelectedIndex();
        text = new StringBuffer(functionNames.get(index)).append("( ");
        for (int i = 1; i < noOfVariables.get(index); i++) {
            text.append(", ");
        }        
        text.append(")");
        jTextArea2.setText(descriptions.get(index));
        jTextArea2.setCaretPosition(0);
        jTextArea1.setText(text.toString());
        jTextArea1.setCaretPosition(0);
    }

    @Override
    protected final void update() {
        text = new StringBuffer(jTextArea1.getText());
        commit();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextArea2 = new javax.swing.JTextArea();
        jLabel3 = new javax.swing.JLabel();

        setClosable(true);
        setTitle("Use Function");

        jButton1.setText("OK");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel1.setText("Name          : ");

        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });

        jLabel2.setText("Expression : ");

        jTextArea1.setColumns(30);
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        jTextArea2.setColumns(30);
        jTextArea2.setEditable(false);
        jTextArea2.setRows(3);
        jScrollPane2.setViewportView(jTextArea2);

        jLabel3.setText("Description : ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 402, Short.MAX_VALUE)
                            .addComponent(jComboBox1, 0, 402, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(23, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        update();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        updateSelection();
    }//GEN-LAST:event_jComboBox1ItemStateChanged
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTextArea jTextArea2;
    // End of variables declaration//GEN-END:variables
}
