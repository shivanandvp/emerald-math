package UI.Dialog;

import javax.swing.JOptionPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.JTextComponent;

public abstract class TextReplacerFrame extends javax.swing.JInternalFrame {

    protected StringBuffer text;
    protected JTextComponent jTextComponent;

    protected abstract void update();

    protected final void commit(){
        try {
            jTextComponent.getDocument().insertString(jTextComponent.getCaret().getDot(), text.toString(), null);
        } catch (BadLocationException ex) {
            JOptionPane.showInternalMessageDialog(Data.Objects.desktopPane, ex, "Error : Text could not be inserted.", JOptionPane.ERROR_MESSAGE);
        }       
        this.dispose();
    }

    public TextReplacerFrame(JTextComponent uJTextComponent) {
        jTextComponent = uJTextComponent;
        text = new StringBuffer();
    }
}
