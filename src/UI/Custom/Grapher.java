package UI.Custom;

import Core.Function;
import Data.Defaults;
import Data.Objects;
import Graph.Graph;
import Graph.Graph2D;
import Graph.Graph3D;
import UI.Dialog.FunctionChooser;
import java.util.ArrayList;

public class Grapher extends javax.swing.JInternalFrame {

    public Grapher() {
        initComponents();

        setLocation(100, 100);
        setParametricVisible(false);

        jComboBox1.addItem("Cartesian 2D y(x)");
        jComboBox1.addItem("Cartesian 2D parametric x(t), y(t)");
        jComboBox1.addItem("Polar 2D r(theta)");
        jComboBox1.addItem("Polar 2D parametric r(t), theta(t)");
        jComboBox1.addItem("Cartesian 3D z(x, y)");
        jComboBox1.addItem("Cartesian 3D y(x, z)");
        jComboBox1.addItem("Cartesian 3D x(y, z)");
        jComboBox1.addItem("Cylindrical 3D rho(phi, z)");
        jComboBox1.addItem("Cylindrical 3D phi(rho, z)");
        jComboBox1.addItem("Cylindrical 3D z(rho, phi)");
        jComboBox1.addItem("Spherical 3D r(theta, phi)");
        jComboBox1.addItem("Spherical 3D theta(r, phi)");
        jComboBox1.addItem("Spherical 3D phi(r, theta)");
        jComboBox1.setSelectedIndex(0);
    }

    private final void setParametricVisible(boolean state) {        
            jTextPane2.setVisible(state);
            jScrollPane2.setVisible(state);
            jTextField2.setVisible(state);
            jLabel3.setVisible(state);
            jButton3.setVisible(state);
            pack();        
    }

    private final void createGraphFunction() {
        int index = jComboBox1.getSelectedIndex();
        ArrayList<String> parameterNames = new ArrayList<String>(1);
        ArrayList<String> parameterNames1 = new ArrayList<String>(1);
        Graph graph = null;
        switch (index) {
            case 0:
                parameterNames.add("x");
                graph = Data.Defaults.Graph2D.assignTemplate(new Graph2D(new Function("f", jTextPane1.getText(), parameterNames), Graph2D.CoordType.CARTESIAN), Defaults.Graph2DType.CARTESIAN_DEFAULT);
                break;
            case 1:
                parameterNames.add("t");
                parameterNames1.add("t");
                graph = Data.Defaults.Graph2D.assignTemplate(new Graph2D(new Function("f", jTextPane1.getText(), parameterNames), new Function("g", jTextPane2.getText(), parameterNames1), Graph2D.CoordType.CARTESIAN), Defaults.Graph2DType.CARTESIAN_DEFAULT);
                break;
            case 2:
                parameterNames.add("theta");
                graph = Data.Defaults.Graph2D.assignTemplate(new Graph2D(new Function("f", jTextPane1.getText(), parameterNames), Graph2D.CoordType.POLAR), Defaults.Graph2DType.POLAR_DEFAULT);
                break;
            case 3:
                parameterNames.add("t");
                parameterNames1.add("t");
                graph = Data.Defaults.Graph2D.assignTemplate(new Graph2D(new Function("f", jTextPane1.getText(), parameterNames), new Function("g", jTextPane2.getText(), parameterNames1), Graph2D.CoordType.POLAR), Defaults.Graph2DType.POLAR_DEFAULT);
                break;
            case 4:
                parameterNames.add("x");
                parameterNames.add("y");
                graph = Data.Defaults.Graph3D.assignTemplate(new Graph3D(new Function("f", jTextPane1.getText(), parameterNames), Graph3D.CoordType.CARTESIAN_FUNCTION_Z), Defaults.Graph3DType.CARTESIAN_DEFAULT);
                break;
            case 5:
                parameterNames.add("x");
                parameterNames.add("z");
                graph = Data.Defaults.Graph3D.assignTemplate(new Graph3D(new Function("f", jTextPane1.getText(), parameterNames), Graph3D.CoordType.CARTESIAN_FUNCTION_Y), Defaults.Graph3DType.CARTESIAN_DEFAULT);
                break;
            case 6:
                parameterNames.add("y");
                parameterNames.add("z");
                graph = Data.Defaults.Graph3D.assignTemplate(new Graph3D(new Function("f", jTextPane1.getText(), parameterNames), Graph3D.CoordType.CARTESIAN_FUNCTION_X), Defaults.Graph3DType.CARTESIAN_DEFAULT);
                break;
            case 7:
                parameterNames.add("phi");
                parameterNames.add("z");
                graph = Data.Defaults.Graph3D.assignTemplate(new Graph3D(new Function("f", jTextPane1.getText(), parameterNames), Graph3D.CoordType.CYLINDRICAL_FUNCTION_RHO), Defaults.Graph3DType.CYLINDRICAL_RHO);
                break;
            case 8:
                parameterNames.add("rho");
                parameterNames.add("z");
                graph = Data.Defaults.Graph3D.assignTemplate(new Graph3D(new Function("f", jTextPane1.getText(), parameterNames), Graph3D.CoordType.CYLINDRICAL_FUNCTION_PHI), Defaults.Graph3DType.CYLINDRICAL_PHI);
                break;
            case 9:
                parameterNames.add("rho");
                parameterNames.add("phi");
                graph = Data.Defaults.Graph3D.assignTemplate(new Graph3D(new Function("f", jTextPane1.getText(), parameterNames), Graph3D.CoordType.CYLINDRICAL_FUNCTION_Z), Defaults.Graph3DType.CYLINDRICAL_Z);
                break;
            case 10:
                parameterNames.add("theta");
                parameterNames.add("phi");
                graph = Data.Defaults.Graph3D.assignTemplate(new Graph3D(new Function("f", jTextPane1.getText(), parameterNames), Graph3D.CoordType.SPHERICAL_FUNCTION_R), Defaults.Graph3DType.SPHERICAL_R);
                break;
            case 11:
                parameterNames.add("r");
                parameterNames.add("phi");
                graph = Data.Defaults.Graph3D.assignTemplate(new Graph3D(new Function("f", jTextPane1.getText(), parameterNames), Graph3D.CoordType.SPHERICAL_FUNCTION_THETA), Defaults.Graph3DType.SPHERICAL_THETA);
                break;
            case 12:
                parameterNames.add("r");
                parameterNames.add("theta");
                graph = Data.Defaults.Graph3D.assignTemplate(new Graph3D(new Function("f", jTextPane1.getText(), parameterNames), Graph3D.CoordType.SPHERICAL_FUNCTION_PHI), Defaults.Graph3DType.SPHERICAL_PHI);
                break;
        }
        if (graph != null) {
            GraphInternalFrame graphInternalFrame = new GraphInternalFrame(graph, null);
            Objects.desktopPane.add(graphInternalFrame);
            Objects.graphInternalFrames.add(graphInternalFrame);
        }
        this.dispose();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jComboBox1 = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jLabel2 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane2 = new javax.swing.JTextPane();
        jLabel3 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();

        setClosable(true);
        setIconifiable(true);
        setTitle("Grapher");

        jComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox1ItemStateChanged(evt);
            }
        });

        jScrollPane1.setViewportView(jTextPane1);

        jLabel2.setText("=");

        jTextField1.setEditable(false);

        jButton1.setText("Use a function in text");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("Done");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jScrollPane2.setViewportView(jTextPane2);

        jLabel3.setText("=");

        jTextField2.setEditable(false);

        jButton3.setText("Use a function in text");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jLabel1.setText("Choose a suitable graph type from the drop down menu below : ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jComboBox1, 0, 374, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addGap(277, 277, 277))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 374, Short.MAX_VALUE)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextField2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox1ItemStateChanged
        int index = jComboBox1.getSelectedIndex();
        setParametricVisible(false);
        switch (index) {
            case 0:
                jTextField1.setText("y(x)");
                break;
            case 1:
                jTextField1.setText("x(t)");
                jTextField2.setText("y(t)");
                setParametricVisible(true);
                break;
            case 2:
                jTextField1.setText("r(theta)");
                break;
            case 3:
                jTextField1.setText("r(t)");
                jTextField2.setText("theta(t)");
                setParametricVisible(true);
                break;
            case 4:
                jTextField1.setText("z(x, y)");
                break;
            case 5:
                jTextField1.setText("y(x, z)");
                break;
            case 6:
                jTextField1.setText("x(y, z)");
                break;
            case 7:
                jTextField1.setText("rho(phi, z)");
                break;
            case 8:
                jTextField1.setText("phi(rho, z)");
                break;
            case 9:
                jTextField1.setText("z(rho, phi)");
                break;
            case 10:
                jTextField1.setText("r(theta, phi)");
                break;
            case 11:
                jTextField1.setText("theta(r, phi)");
                break;
            case 12:
                jTextField1.setText("phi(r, theta)");
                break;
        }
    }//GEN-LAST:event_jComboBox1ItemStateChanged

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        FunctionChooser functionChooser = new FunctionChooser(jTextPane1);
        Data.Objects.desktopPane.add(functionChooser);
        functionChooser.show();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        createGraphFunction();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        FunctionChooser functionChooser = new FunctionChooser(jTextPane2);
        Data.Objects.desktopPane.add(functionChooser);
        functionChooser.show();
    }//GEN-LAST:event_jButton3ActionPerformed
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextPane jTextPane1;
    private javax.swing.JTextPane jTextPane2;
    // End of variables declaration//GEN-END:variables
}
