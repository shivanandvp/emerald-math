package UI.Custom;

public final class ProcessGraphsThread extends Thread {    

    public ProcessGraphsThread() {
        super();        
    }

    @Override
    public final void run() {
        Core.Evaluator.processGraphsAction();
    }
}
