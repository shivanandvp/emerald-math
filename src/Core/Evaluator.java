package Core;

import Data.Defaults;
import Data.Objects;
import Graph.Graph;
import Graph.Graph2D;
import Graph.Graph3D;
import UI.Custom.GraphInternalFrame;
import java.util.ArrayList;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Evaluator {

    protected static final String tokenSeparator = " ", statementSeperator = ";", openBracket = "(", closeBracket = ")";
    protected static final char tokenSeparatorChar = ' ', statementSeperatorChar = ';', openBracketChar = '(', closeBracketChar = ')';
    protected static final String commentString = "//";

    /* Protected methods */
    protected static final void checkExpression(StringBuffer expression) throws ExpressionParseException {
        int bracketCount = 0;
        for (int i = 0; i < expression.length(); i++) {
            if (expression.charAt(i) == openBracketChar) {
                bracketCount++;
            } else if (expression.charAt(i) == closeBracketChar) {
                bracketCount--;
                if (bracketCount < 0) {
                    throw new ExpressionParseException("Extra closed bracket ", i, expression.toString());
                }
            }
        }
        if (bracketCount > 0) {
            throw new ExpressionParseException("Closed bracket missing", expression.length(), expression.toString());
        }
    }

    /* Public methods */
    public static void evaluateUserInput(String userInput) throws ExpressionParseException {
        /* Remove whitespaces */
        StringBuffer input = new StringBuffer(userInput);
        for (int i = 0; i < input.length(); i++) {
            if (Character.isWhitespace(input.charAt(i))) {
                input.deleteCharAt(i);
                --i;
            }
        }

        Objects.globalFunctions = new ArrayList<Function>();
        Objects.globalMatrices2D = new ArrayList<Matrix2D>();
        Objects.globalVariables = new ArrayList<Variable>();
        Objects.graphs = new ArrayList<Graph>();

        StringTokenizer stringTokenizer = new StringTokenizer(input.toString(), statementSeperator, false);
        String token = "";
        while (stringTokenizer.hasMoreTokens()) {
            token = stringTokenizer.nextToken();
            if (token.startsWith(commentString)) {
                continue;
            }
            if (token.indexOf("=") != -1) {
                /* Function */ if (token.indexOf(openBracket) != -1 && token.indexOf(openBracket) < token.indexOf("=")) {
                    StringTokenizer variableTokenizer = new StringTokenizer(token.substring(token.indexOf(openBracket) + 1, token.indexOf(closeBracket)), ",", false);
                    ArrayList<String> variableNames = new ArrayList<String>();
                    while (variableTokenizer.hasMoreTokens()) {
                        variableNames.add(variableTokenizer.nextToken());
                    }
                    Function function = new Function(token.substring(0, token.indexOf(openBracket)), token.substring(token.indexOf("=") + 1), variableNames);
                    function.calculatePostfix();
                    Objects.globalFunctions.add(function);
                } /* Matrix */ else if (token.indexOf("(") != -1 && token.substring(token.indexOf("=") + 1, token.indexOf(openBracket)).equalsIgnoreCase("matrix2D")) {
                    Matrix2D matrix2D = new Matrix2D(token.substring(0, token.indexOf('[')), token.substring(token.indexOf("=") + 1), Integer.parseInt(token.substring(token.indexOf("[") + 1, token.indexOf("]"))), Integer.parseInt(token.substring(token.lastIndexOf("[") + 1, token.lastIndexOf("]"))));
                    matrix2D.calculatePostfix();
                    Objects.globalMatrices2D.add(matrix2D);
                } /* Variable */ else {
                    Variable variable = new Variable(token.substring(0, token.indexOf("=")), token.substring(token.indexOf("=") + 1));
                    variable.calculatePostfix();
                    variable.evaluate();
                    Objects.globalVariables.add(variable);
                }
            } /* Graph */ else {
                String name = token.substring(0, token.indexOf(openBracket)), name1, name2;
                if (token.indexOf(",") == -1) {
                    name1 = token.substring(token.indexOf(openBracket) + 1, token.lastIndexOf(closeBracket));
                    if (name.equalsIgnoreCase("cartesianGraph")) {
                        for (Function function : Objects.globalFunctions) {
                            if (function.getName().equalsIgnoreCase(name1)) {
                                Objects.graphs.add(Data.Defaults.Graph2D.assignTemplate(new Graph2D(function, Graph2D.CoordType.CARTESIAN), Defaults.Graph2DType.CARTESIAN_DEFAULT));
                            }
                        }
                    } else if (name.equalsIgnoreCase("polarGraph")) {
                        for (Function function : Objects.globalFunctions) {
                            if (function.getName().equalsIgnoreCase(name1)) {
                                Objects.graphs.add(Data.Defaults.Graph2D.assignTemplate(new Graph2D(function, Graph2D.CoordType.POLAR), Defaults.Graph2DType.POLAR_DEFAULT));
                            }
                        }
                    } else if (name.equalsIgnoreCase("cartesian3DXGraph")) {
                        for (Function function : Objects.globalFunctions) {
                            if (function.getName().equalsIgnoreCase(name1)) {
                                Objects.graphs.add(Data.Defaults.Graph3D.assignTemplate(new Graph3D(function, Graph3D.CoordType.CARTESIAN_FUNCTION_X), Defaults.Graph3DType.CARTESIAN_DEFAULT));
                            }
                        }
                    } else if (name.equalsIgnoreCase("cartesian3DYGraph")) {
                        for (Function function : Objects.globalFunctions) {
                            if (function.getName().equalsIgnoreCase(name1)) {
                                Objects.graphs.add(Data.Defaults.Graph3D.assignTemplate(new Graph3D(function, Graph3D.CoordType.CARTESIAN_FUNCTION_Y), Defaults.Graph3DType.CARTESIAN_DEFAULT));
                            }
                        }
                    } else if (name.equalsIgnoreCase("cartesian3DZGraph")) {
                        for (Function function : Objects.globalFunctions) {
                            if (function.getName().equalsIgnoreCase(name1)) {
                                Objects.graphs.add(Data.Defaults.Graph3D.assignTemplate(new Graph3D(function, Graph3D.CoordType.CARTESIAN_FUNCTION_Z), Defaults.Graph3DType.CARTESIAN_DEFAULT));
                            }
                        }
                    } else if (name.equalsIgnoreCase("cylindrical3DRhoGraph")) {
                        for (Function function : Objects.globalFunctions) {
                            if (function.getName().equalsIgnoreCase(name1)) {
                                Objects.graphs.add(Data.Defaults.Graph3D.assignTemplate(new Graph3D(function, Graph3D.CoordType.CYLINDRICAL_FUNCTION_RHO), Defaults.Graph3DType.CYLINDRICAL_RHO));
                            }
                        }
                    } else if (name.equalsIgnoreCase("cylindrical3DPhiGraph")) {
                        for (Function function : Objects.globalFunctions) {
                            if (function.getName().equalsIgnoreCase(name1)) {
                                Objects.graphs.add(Data.Defaults.Graph3D.assignTemplate(new Graph3D(function, Graph3D.CoordType.CYLINDRICAL_FUNCTION_PHI), Defaults.Graph3DType.CYLINDRICAL_PHI));
                            }
                        }
                    } else if (name.equalsIgnoreCase("cylindrical3DZGraph")) {
                        for (Function function : Objects.globalFunctions) {
                            if (function.getName().equalsIgnoreCase(name1)) {
                                Objects.graphs.add(Data.Defaults.Graph3D.assignTemplate(new Graph3D(function, Graph3D.CoordType.CYLINDRICAL_FUNCTION_Z), Defaults.Graph3DType.CYLINDRICAL_Z));
                            }
                        }
                    } else if (name.equalsIgnoreCase("Spherical3DRGraph")) {
                        for (Function function : Objects.globalFunctions) {
                            if (function.getName().equalsIgnoreCase(name1)) {
                                Objects.graphs.add(Data.Defaults.Graph3D.assignTemplate(new Graph3D(function, Graph3D.CoordType.SPHERICAL_FUNCTION_R), Defaults.Graph3DType.SPHERICAL_R));
                            }
                        }
                    } else if (name.equalsIgnoreCase("Spherical3DThetaGraph")) {
                        for (Function function : Objects.globalFunctions) {
                            if (function.getName().equalsIgnoreCase(name1)) {
                                Objects.graphs.add(Data.Defaults.Graph3D.assignTemplate(new Graph3D(function, Graph3D.CoordType.SPHERICAL_FUNCTION_THETA), Defaults.Graph3DType.SPHERICAL_THETA));
                            }
                        }
                    } else if (name.equalsIgnoreCase("Spherical3DPhiGraph")) {
                        for (Function function : Objects.globalFunctions) {
                            if (function.getName().equalsIgnoreCase(name1)) {
                                Objects.graphs.add(Data.Defaults.Graph3D.assignTemplate(new Graph3D(function, Graph3D.CoordType.SPHERICAL_FUNCTION_PHI), Defaults.Graph3DType.SPHERICAL_PHI));
                            }
                        }
                    }
                } else {
                    name1 = token.substring(token.indexOf(openBracket) + 1, token.indexOf(","));
                    name2 = token.substring(token.indexOf(",") + 1, token.lastIndexOf(closeBracket));
                    Function function1 = null, function2 = null;
                    for (Function function : Objects.globalFunctions) {
                        if (function.getName().equalsIgnoreCase(name1)) {
                            function1 = function;
                        } else if (function.getName().equalsIgnoreCase(name2)) {
                            function2 = function;
                        }
                    }
                    if (function1 == null || function2 == null) {
                        return;
                    }
                    if (name.equalsIgnoreCase("cartesianGraph")) {
                        Objects.graphs.add(Data.Defaults.Graph2D.assignTemplate(new Graph2D(function1, function2, Graph2D.CoordType.CARTESIAN), Defaults.Graph2DType.CARTESIAN_DEFAULT));
                    } else if (name.equalsIgnoreCase("polarGraph")) {
                        Objects.graphs.add(Data.Defaults.Graph2D.assignTemplate(new Graph2D(function1, function2, Graph2D.CoordType.POLAR), Defaults.Graph2DType.POLAR_DEFAULT));
                    }
                }
            }
        }
        Data.Objects.statusBar.setText("User input evaluated.");
    }

    public static final void processGraphs() {
        if (Data.Objects.processGraphsThread == null || !Data.Objects.processGraphsThread.isAlive()) {
            Data.Objects.processGraphsThread = new UI.Custom.ProcessGraphsThread();
            Data.Objects.processGraphsThread.start();
        } else if (Data.Objects.processGraphsThread.isAlive()) {
            try {
                Data.Objects.processGraphsThread.join();
                Data.Objects.processGraphsThread = new UI.Custom.ProcessGraphsThread();
                Data.Objects.processGraphsThread.start();
            } catch (InterruptedException ex) {
                Logger.getLogger(Evaluator.class.getName()).log(Level.SEVERE, "InterruptedException in method processGraphs", ex);
            }
        }
    }

    public static final void processGraphsAction() {
        Objects.stopFlag = false;
        int graphsCount = 0;
        for (Graph graph : Objects.graphs) {
            GraphInternalFrame graphInternalFrame = new GraphInternalFrame(graph, null);
            Objects.desktopPane.add(graphInternalFrame);
            Objects.graphInternalFrames.add(graphInternalFrame);
            ++graphsCount;
            Data.Objects.statusBar.setText("Number of Graphs processed : " + graphsCount);
        }
        Data.Objects.statusBar.setText("All Graphs Processed. Total number of Graphs : " + graphsCount);
    }

    /* Exceptions */
    public static final class ExpressionParseException extends java.text.ParseException {

        private String expression;
        private int linePos;

        public ExpressionParseException(String uMessage, int uRowPos, String uExpression) {
            super(uMessage, uRowPos);
            expression = uExpression;
        }

        public final String getExpression() {
            return expression;
        }

        public final int getLinePos() {
            return linePos;
        }

        @Override
        public String toString() {
            return getMessage() + " in expression \" " + expression + " \"";
        }
    }
}
