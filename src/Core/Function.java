package Core;

import java.util.ArrayList;

public class Function {

    private ArrayList<Variable> parameters;
    private String name, string;
    private StringBuffer postfix;

    /* Constructors */
    public Function() {
        name = "";
        string = "";
        parameters = new ArrayList<Variable>();
    }

    public Function(String uName) {
        name = uName;
        string = "";
        parameters = new ArrayList<Variable>();
    }

    public Function(String uName, String uString) {
        name = uName;
        string = uString;
        parameters = new ArrayList<Variable>();
    }

    public Function(String uName, ArrayList<String> uParameterNames) {
        name = uName;
        string = "";
        parameters = new ArrayList<Variable>();
        for (String parameterName : uParameterNames) {
            parameters.add(new Variable(parameterName, 0.0));
        }
    }

    public Function(String uName, String uString, ArrayList<String> uParameterNames) {
        name = uName;
        string = uString;
        parameters = new ArrayList<Variable>();
        for (String parameterName : uParameterNames) {
            parameters.add(new Variable(parameterName, 0.0));
        }
    }

    /* Get methods */
    public final ArrayList<Variable> getParameters() {
        return parameters;
    }

    public final String getName() {
        return name;
    }

    public final String getString() {
        return string;
    }

    public final StringBuffer getPostfix() {
        return postfix;
    }

    public final int getNumberOfParameters() {
        return parameters.size();
    }

    public final boolean hasSingleParameter() {
        return (parameters.size() == 1);
    }

    public final boolean hasTwoParameters() {
        return (parameters.size() == 2);
    }

    /* Set methods */
    public final void setName(String uName) {
        name = uName;
    }

    public final void setString(String uString) {
        string = uString;
    }

    /* Processing methods */
    public final void calculatePostfix() throws ShuntingStackEvaluator.ExpressionParseException {
        postfix = ShuntingStackEvaluator.convertInfixToPostfix(ShuntingStackEvaluator.convertExpressionToInfix(string));
    }

    public double evaluate(ArrayList<Double> uValues) {
        for (int i = 0; i < parameters.size(); i++) {
            parameters.get(i).setValue(uValues.get(i));
        }
        return ShuntingStackEvaluator.evaluatePostfix(postfix.toString(), parameters);
    }
}
