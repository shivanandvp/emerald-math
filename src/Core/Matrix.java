package Core;

public class Matrix {

    protected double dimensions;
    protected String name, string;
    protected StringBuffer postfix;

    /* Get Methods */
    public final String getName() {
        return name;
    }

    public final String getString() {
        return string;
    }

    public final StringBuffer getPostfix() {
        return postfix;
    }

    public final double getDimensions() {
        return dimensions;
    }

    /* Set methods */
    public final void setName(String uName) {
        name = uName;
    }

    public final void setString(String uString) {
        string = uString;
    }

    public final void setDimensions(double uDimensions) {
        dimensions = uDimensions;
    }
}
