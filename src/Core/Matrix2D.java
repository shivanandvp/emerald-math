package Core;

import Core.Evaluator.ExpressionParseException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Matrix2D extends Matrix {

    private int m = 0, n = 0;

    /* Constructors */
    public Matrix2D() {
        dimensions = 2;
        name = "";
        string = "";
    }

    public Matrix2D(String uName, int uM, int uN) {
        dimensions = 2;
        name = uName;
        string = "";
        m = uM;
        n = uN;
    }

    public Matrix2D(String uName, String uString, int uM, int uN) {
        dimensions = 2;
        name = uName;
        string = uString;
        m = uM;
        n = uN;
    }

    /* Get methods */
    public final boolean isASquareMatrix() {
        return (m == n);
    }

    public final int getNumRows() {
        return m;
    }

    public final int getNumColumns() {
        return n;
    }

    /* Set methods */
    public final void setNumRows(int uM) {
        m = uM;
    }

    public final void setNumColumns(int uN) {
        n = uN;
    }

    /* Processing methods */
    public static double determinant(double matrix[][], int r, int c) {
        double d = 0;
        if (r != c) {
            return Double.NaN;
        } else if (r <= 1) {
            return matrix[0][0];
        }
        double[][] minor = new double[r - 1][c - 1];
        int xCount = 0, yCount = 0;
        for (int i = 0; i < c; i++) {
            for (int x = 0; x < r; x++) {
                for (int y = 0; y < c; y++) {
                    if (x != 0 && y != i) {
                        if (yCount >= (c - 1)) {
                            yCount = 0;
                            xCount++;
                        }
                        minor[xCount][yCount++] = matrix[x][y];
                    }
                }
            }
            if (i % 2 == 0) {
                d += matrix[0][i] * determinant(minor, r - 1, c - 1);
            } else {
                d -= matrix[0][i] * determinant(minor, r - 1, c - 1);
            }
            xCount = 0;
            yCount = 0;
        }
        return d;
    }

    public final void calculatePostfix() throws ExpressionParseException {
        postfix = ShuntingStackEvaluator.convertInfixToPostfix(ShuntingStackEvaluator.convertExpressionToInfix(string));
    }

    public double evaluateDeterminant() {
        StringBuffer tempString = new StringBuffer(postfix.substring(postfix.indexOf(Evaluator.openBracket) + 1, postfix.lastIndexOf(Evaluator.closeBracket)));
        {
            int bracketCount = 0;
            for (int i = 0; i < tempString.length(); i++) {
                switch (tempString.charAt(i)) {
                    case ',':
                        if (bracketCount == 0) {
                            tempString.replace(i, i + 1, ShuntingStackEvaluator.getCommaReplacer());
                        }
                        continue;
                    case Evaluator.openBracketChar:
                        bracketCount++;
                        continue;
                    case Evaluator.closeBracketChar:
                        bracketCount--;
                        continue;
                }
            }
        }
        StringTokenizer commaTokenizer = new StringTokenizer(tempString.toString(), String.valueOf(ShuntingStackEvaluator.getCommaReplacer()), false);
        double[] matrix = new double[m * n];
        int count = 0;
        while (commaTokenizer.hasMoreTokens()) {
            matrix[count++] = ShuntingStackEvaluator.evaluatePostfix(commaTokenizer.nextToken(), new ArrayList<Variable>());
        }
        double[][] matrix2D = new double[m][n];
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                matrix2D[i][j] = matrix[i * m + j];
            }
        }
        return determinant(matrix2D, m, n);
    }
}
