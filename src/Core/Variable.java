package Core;

import java.util.ArrayList;

public class Variable {

    private String name, string;
    private StringBuffer postfix;
    private double value;

    /* Constructors */
    public Variable() {
        name = "";
        value = Double.NaN;
        string = "";
    }

    public Variable(String uName) {
        name = uName;
        value = Double.NaN;
        string = "";
    }

    public Variable(String uName, double uValue) {
        name = uName;
        value = uValue;
        string = "";
    }

    public Variable(String uName, String uString) {
        name = uName;
        string = uString;
        value = Double.NaN;
    }

    /* Get methods */
    public final String getName() {
        return name;
    }

    public final String getString() {
        return string;
    }

    public final StringBuffer getPostfix() {
        return postfix;
    }

    public final double getValue() {
        return value;
    }

    /* Set methods */
    public final void setName(String uName) {
        name = uName;
    }

    public final void setString(String uString) {
        string = uString;
    }

    public final void setValue(double uValue) {
        value = uValue;
    }

    /* Processing methods */
    public final void calculatePostfix() throws ShuntingStackEvaluator.ExpressionParseException {
        postfix = ShuntingStackEvaluator.convertInfixToPostfix(ShuntingStackEvaluator.convertExpressionToInfix(string));
    }

    public double evaluate() {
        value = ShuntingStackEvaluator.evaluatePostfix(postfix.toString(), new ArrayList<Variable>());
        return value;
    }
}
