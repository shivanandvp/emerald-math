package Core;

import Data.Objects;
import java.util.ArrayList;
import java.util.Stack;
import java.util.StringTokenizer;

public final class ShuntingStackEvaluator extends Evaluator {

    private static final String commaReplacer = "$", separatorReplacer = "|", negativeReplacer = "~";

    /* Private methods */
    private static final int getPrecedence(char operator) {
        switch (operator) {
            case '+':
                return 1;
            case '-':
                return 1;
            case '*':
                return 2;
            case '/':
                return 2;
            case openBracketChar:
                return -1;
            case closeBracketChar:
                return -2;
            case '%':
                return 2;
            case '^':
                return 3;
            case '!':
                return 4;
            case ',':
                return -3;
        }
        return 0;
    }

    /* Get methods */
    public static final String getCommaReplacer() {
        return commaReplacer;
    }

    public static final String getSeparatorReplacer() {
        return separatorReplacer;
    }

    public static final String getNegativeReplacer() {
        return negativeReplacer;
    }

    /* Public methods */
    public static final StringBuffer convertExpressionToInfix(String expression) throws ExpressionParseException {
        StringBuffer infix = new StringBuffer(openBracket).append(expression).append(closeBracket);
        for (int i = 1; i < infix.length() - 1; i++) {
            if (Character.isWhitespace(infix.charAt(i))) {
                infix.deleteCharAt(i);
                --i;
            }
        }
        checkExpression(infix);
        return infix;
    }

    public static final StringBuffer convertInfixToPostfix(StringBuffer infix) {
        int operatorCount = 0;
        int[] operatorPos;
        int[] precedence;
        {
            int length = infix.length();
            operatorPos = new int[length];
            precedence = new int[length];
            operatorPos[operatorCount++] = 0;
            precedence[0] = -1;
            boolean inFunctionBrackets = false;
            int bracketsCount = 0, functionBracketsCount = 0;
            for (int i = 1; i < length; i++) {
                precedence[i] = getPrecedence(infix.charAt(i));
                /* A character */ if (precedence[i] == 0) {
                    continue;
                } /* An operator */ else if (!inFunctionBrackets && precedence[i] > 0) {
                    if (infix.charAt(i) != '-') {
                        operatorPos[operatorCount++] = i;
                        continue;
                    } else {
                        if (infix.charAt(i - 1) == openBracketChar || precedence[i - 1] > 0) {
                            infix.replace(i, i + 1, negativeReplacer);
                            continue;
                        } else {
                            operatorPos[operatorCount++] = i;
                            continue;
                        }
                    }
                }
                if (precedence[i] > 0) {
                    continue;
                }
                switch (precedence[i]) {
                    /* An open bracket */ case -1:
                        /* A normal bracket */ if (!inFunctionBrackets && precedence[i - 1] != 0) {
                            operatorPos[operatorCount++] = i;
                            bracketsCount++;
                        } /* A function bracket */ else {
                            inFunctionBrackets = true;
                            functionBracketsCount++;
                            bracketsCount++;
                        }
                        continue;
                    /* A closed bracket */ case -2:
                        /* A normal bracket */ if (!inFunctionBrackets) {
                            operatorPos[operatorCount++] = i;
                            bracketsCount--;
                        }/* A function bracket */ else {
                            functionBracketsCount--;
                            if (functionBracketsCount <= 0) {
                                inFunctionBrackets = false;
                            }
                        }
                        continue;
                    /* A comma */ case -3:
                        if (inFunctionBrackets && functionBracketsCount == 1) {
                            infix.replace(i, i + 1, commaReplacer);
                        }
                        continue;
                    default:
                        continue;
                }
            }
        }
        StringBuffer operator, operand, postfix = new StringBuffer();
        Stack<StringBuffer> postfixStack = new Stack<StringBuffer>();
        for (int i = 0; i < (operatorCount - 1); i++) {
            operator = new StringBuffer().append(infix.charAt(operatorPos[i]));
            operand = new StringBuffer(infix.substring(operatorPos[i] + 1, operatorPos[i + 1]));
            if (operand.indexOf(openBracket) > 0) {
                StringTokenizer commaTokenizer = new StringTokenizer(operand.substring(operand.indexOf(openBracket) + 1, operand.lastIndexOf(closeBracket)), commaReplacer, false);
                operand = new StringBuffer(operand.substring(0, operand.indexOf(openBracket) + 1));
                while (commaTokenizer.hasMoreTokens()) {
                    operand.append(convertInfixToPostfix(new StringBuffer(openBracket).append(commaTokenizer.nextToken()).append(closeBracket))).append(",");
                }
                operand.deleteCharAt(operand.length() - 1);
                operand.append(closeBracket);
            }
            /* An operator */ if (precedence[operatorPos[i]] > 0) {
                while (precedence[operatorPos[i]] <= getPrecedence(postfixStack.peek().charAt(0))) {
                    postfix.append(tokenSeparator).append(postfixStack.pop());
                }
                postfixStack.push(operator);
            } /* An open bracket */ else if (precedence[operatorPos[i]] == -1) {
                postfixStack.push(operator);
            } /* A close bracket */ else {
                while (postfixStack.peek().charAt(0) != openBracketChar) {
                    postfix.append(tokenSeparator).append(postfixStack.pop());
                }
                postfixStack.pop();
            }
            if (operand.length() != 0) {
                postfix.append(tokenSeparator).append(operand);
            }
        }
        while (postfixStack.peek().charAt(0) != openBracketChar) {
            postfix.append(tokenSeparator).append(postfixStack.pop());
        }
        postfix.append(tokenSeparator);
        return postfix;
    }

    public static final double evaluatePostfix(String postfix, ArrayList<Variable> parameters) {
        {
            StringBuffer temp = new StringBuffer(postfix);
            int bracketCount = 0;
            for (int i = 0; i < temp.length(); i++) {
                if (temp.charAt(i) == negativeReplacer.charAt(0)) {
                    temp.replace(i, i + 1, "-");
                }
            }
            for (int i = 0; i < temp.length(); i++) {
                switch (temp.charAt(i)) {
                    case tokenSeparatorChar:
                        if (bracketCount <= 0) {
                            temp.replace(i, i + 1, separatorReplacer);
                        }
                        continue;
                    case openBracketChar:
                        bracketCount++;
                        continue;
                    case closeBracketChar:
                        bracketCount--;
                        continue;
                }
            }
            postfix = temp.toString();
        }
        Stack<String> answerStack = new Stack<String>();
        answerStack.push("0.0");
        double operand1, operand2;
        String token = "";
        StringTokenizer expressionTokenizer = new StringTokenizer(String.valueOf(postfix), separatorReplacer, false);
        while (expressionTokenizer.hasMoreTokens()) {
            token = expressionTokenizer.nextToken();
            /* Number */ if (Character.isDigit(token.charAt(0)) || (token.charAt(0) == '-' && token.length() != 1)) {
                answerStack.push(token);
            } /* Operator */ else if (!Character.isLetterOrDigit(token.charAt(0))) {
                operand2 = evaluatePostfix(String.valueOf(answerStack.pop()), parameters);
                operand1 = evaluatePostfix(String.valueOf(answerStack.pop()), parameters);
                switch (token.charAt(0)) {
                    case '+':
                        answerStack.push(String.valueOf(operand1 + operand2));
                        break;
                    case '-':
                        answerStack.push(String.valueOf(operand1 - operand2));
                        break;
                    case '*':
                        answerStack.push(String.valueOf(operand1 * operand2));
                        break;
                    case '/':
                        answerStack.push(String.valueOf(operand1 / operand2));
                        break;
                    case '%':
                        answerStack.push(String.valueOf(Math.IEEEremainder(operand1, operand2)));
                        break;
                    case '^':
                        answerStack.push(String.valueOf(Math.pow(operand1, operand2)));
                        break;
                }
            } /* Variable */ else if (token.indexOf(openBracket) == -1) {
                if (token.equalsIgnoreCase("E")) {
                    answerStack.push(String.valueOf(Math.E));
                } else if (token.equalsIgnoreCase("PI")) {
                    answerStack.push(String.valueOf(Math.PI));
                } else {
                    for (Variable variable : parameters) {
                        if (variable.getName().equalsIgnoreCase(token)) {
                            answerStack.push(String.valueOf(variable.getValue()));
                            break;
                        }
                    }
                    for (Variable variable : Objects.globalVariables) {
                        if (variable.getName().equalsIgnoreCase(token)) {
                            answerStack.push(String.valueOf(variable.getValue()));
                            break;
                        }
                    }
                }
            } /* Function */ else {
                String name = token.substring(0, token.indexOf(openBracket));
                {
                    StringBuffer temp = new StringBuffer(token);
                    int bracketCount = 0;
                    for (int i = 0; i < temp.length(); i++) {
                        switch (temp.charAt(i)) {
                            case ',':
                                if (bracketCount == 1) {
                                    temp.replace(i, i + 1, commaReplacer);
                                }
                                continue;
                            case openBracketChar:
                                bracketCount++;
                                continue;
                            case closeBracketChar:
                                bracketCount--;
                                continue;
                        }
                    }
                    token = temp.toString();
                }
                StringTokenizer commaTokenizer = new StringTokenizer(token.substring(token.indexOf(openBracket) + 1, token.lastIndexOf(closeBracket)), String.valueOf(commaReplacer), false);
                ArrayList<Double> arguments = new ArrayList<Double>();
                while (commaTokenizer.hasMoreTokens()) {
                    arguments.add(evaluatePostfix(commaTokenizer.nextToken(), parameters));
                }
                if (name.equalsIgnoreCase("pow")) {
                    answerStack.push(String.valueOf(Math.pow(arguments.get(0), arguments.get(1))));
                } else if (name.equalsIgnoreCase("sqrt")) {
                    answerStack.push(String.valueOf(Math.sqrt(arguments.get(0))));
                } else if (name.equalsIgnoreCase("cbrt")) {
                    answerStack.push(String.valueOf(Math.cbrt(arguments.get(0))));
                } else if (name.equalsIgnoreCase("sin")) {
                    answerStack.push(String.valueOf(Math.sin(arguments.get(0))));
                } else if (name.equalsIgnoreCase("cos")) {
                    answerStack.push(String.valueOf(Math.cos(arguments.get(0))));
                } else if (name.equalsIgnoreCase("tan")) {
                    answerStack.push(String.valueOf(Math.tan(arguments.get(0))));
                } else if (name.equalsIgnoreCase("sinh")) {
                    answerStack.push(String.valueOf((Math.exp(arguments.get(0)) - Math.exp(-arguments.get(0))) / 2));
                } else if (name.equalsIgnoreCase("cosh")) {
                    answerStack.push(String.valueOf((Math.exp(arguments.get(0)) + Math.exp(-arguments.get(0))) / 2));
                } else if (name.equalsIgnoreCase("tanh")) {
                    answerStack.push(String.valueOf(Math.expm1(2 * arguments.get(0)) / (Math.exp(2 * arguments.get(0)) + 1)));
                } else if (name.equalsIgnoreCase("ln")) {
                    answerStack.push(String.valueOf(Math.log(arguments.get(0))));
                } else if (name.equalsIgnoreCase("exp")) {
                    answerStack.push(String.valueOf(Math.exp(arguments.get(0))));
                } else if (name.equalsIgnoreCase("log")) {
                    answerStack.push(String.valueOf(Math.log10(arguments.get(0))));
                } else if (name.equalsIgnoreCase("reci")) {
                    answerStack.push(String.valueOf(1 / arguments.get(0)));
                } else if (name.equalsIgnoreCase("hypot")) {
                    answerStack.push(String.valueOf(Math.hypot(arguments.get(0), arguments.get(1))));
                } else if (name.equalsIgnoreCase("asin")) {
                    answerStack.push(String.valueOf(Math.asin(arguments.get(0))));
                } else if (name.equalsIgnoreCase("acos")) {
                    answerStack.push(String.valueOf(Math.acos(arguments.get(0))));
                } else if (name.equalsIgnoreCase("atan")) {
                    answerStack.push(String.valueOf(Math.atan(arguments.get(0))));
                } else if (name.equalsIgnoreCase("asinh")) {
                    answerStack.push(String.valueOf(Math.log(arguments.get(0) + Math.sqrt(Math.pow(arguments.get(0), 2.0) + 1))));
                } else if (name.equalsIgnoreCase("acosh")) {
                    double det = Math.pow(arguments.get(0), 2.0) - 1;
                    if (det > 0) {
                        answerStack.push(String.valueOf(Math.log(arguments.get(0) + Math.sqrt(det))));
                    } else {
                        answerStack.push("0.0");
                    }
                } else if (name.equalsIgnoreCase("atanh")) {
                    answerStack.push(String.valueOf(0.5 * Math.log((1 + arguments.get(0)) / (1 - arguments.get(0)))));
                } else if (name.equalsIgnoreCase("atan2")) {
                    answerStack.push(String.valueOf(Math.atan2(arguments.get(0), arguments.get(1))));
                } else if (name.equalsIgnoreCase("abs")) {
                    answerStack.push(String.valueOf(Math.abs(arguments.get(0))));
                } else if (name.equalsIgnoreCase("ceil")) {
                    answerStack.push(String.valueOf(Math.ceil(arguments.get(0))));
                } else if (name.equalsIgnoreCase("floor")) {
                    answerStack.push(String.valueOf(Math.floor(arguments.get(0))));
                } else if (name.equalsIgnoreCase("round")) {
                    answerStack.push(String.valueOf(Math.round(arguments.get(0))));
                } else if (name.equalsIgnoreCase("factorial")) {
                    long number = Math.abs(arguments.get(0).longValue()), factorial = 1;
                    for (; number > 0; number--) {
                        factorial *= number;
                    }
                    answerStack.push(String.valueOf(factorial));
                } else if (name.equalsIgnoreCase("nCr")) {
                    long n = Math.abs(arguments.get(0).longValue()), r = Math.abs(arguments.get(1).longValue()), nfact = 1, rfact = 1;
                    long ndifr = Math.abs(n - r), ndifrfact = 1;
                    for (; n > 0; n--) {
                        nfact *= n;
                    }
                    for (; r > 0; r--) {
                        rfact *= r;
                    }
                    for (; ndifr > 0; ndifr--) {
                        ndifrfact *= ndifr;
                    }
                    double ans = nfact / (rfact * ndifrfact);
                    answerStack.push(String.valueOf(ans));
                } else if (name.equalsIgnoreCase("nPr")) {
                    long n = Math.abs(arguments.get(0).longValue()), r = Math.abs(arguments.get(1).longValue()), nfact = 1;
                    long ndifr = Math.abs(n - r), ndifrfact = 1;
                    for (; n > 0; n--) {
                        nfact *= n;
                    }
                    for (; ndifr > 0; ndifr--) {
                        ndifrfact *= ndifr;
                    }
                    double ans = nfact / ndifrfact;
                    answerStack.push(String.valueOf(ans));
                } else if (name.equalsIgnoreCase("random")) {
                    answerStack.push(String.valueOf(arguments.get(0) + Math.random() * Math.abs(arguments.get(1) - arguments.get(0))));
                } else if (name.equalsIgnoreCase("max")) {
                    double max = arguments.get(0);
                    for (int i = 1; i < arguments.size(); i++) {
                        if (max < arguments.get(i)) {
                            max = arguments.get(i);
                        }
                    }
                    answerStack.push(String.valueOf(max));
                } else if (name.equalsIgnoreCase("min")) {
                    double min = arguments.get(0);
                    for (int i = 1; i < arguments.size(); i++) {
                        if (min > arguments.get(i)) {
                            min = arguments.get(i);
                        }
                    }
                    answerStack.push(String.valueOf(min));
                } else if (name.equalsIgnoreCase("sum")) {
                    double ans = 0.0;
                    int start = arguments.get(1).intValue(), end = arguments.get(2).intValue();
                    String exp = token.substring(token.lastIndexOf(commaReplacer) + 1, token.lastIndexOf(closeBracket)).trim();
                    ArrayList<Variable> variables = new ArrayList<Variable>(1);
                    Variable variable = new Variable(token.substring(token.indexOf(openBracket) + 1, token.indexOf(commaReplacer)).trim(), Double.NaN);
                    variables.add(variable);
                    for (int iteration = start; iteration <= end; iteration++) {
                        variable.setValue(iteration);
                        ans += evaluatePostfix(exp, variables);
                    }
                    answerStack.push(String.valueOf(ans));
                } else if (name.equalsIgnoreCase("product")) {
                    double ans = 1.0;
                    int start = arguments.get(1).intValue(), end = arguments.get(2).intValue();
                    String exp = token.substring(token.lastIndexOf(commaReplacer) + 1, token.lastIndexOf(closeBracket)).trim();
                    ArrayList<Variable> variables = new ArrayList<Variable>(1);
                    Variable variable = new Variable(token.substring(token.indexOf(openBracket) + 1, token.indexOf(commaReplacer)).trim(), Double.NaN);
                    variables.add(variable);
                    for (int iteration = start; iteration <= end; iteration++) {
                        variable.setValue(iteration);
                        ans *= evaluatePostfix(exp, variables);
                    }
                    answerStack.push(String.valueOf(ans));
                } else if (name.equalsIgnoreCase("repeat")) {
                    double ans = arguments.get(1).intValue();
                    int num = arguments.get(2).intValue();
                    String exp = token.substring(token.lastIndexOf(commaReplacer) + 1, token.lastIndexOf(closeBracket)).trim();
                    ArrayList<Variable> variables = new ArrayList<Variable>(1);
                    Variable variable = new Variable(token.substring(token.indexOf(openBracket) + 1, token.indexOf(commaReplacer)).trim(), Double.NaN);
                    variables.add(variable);
                    for (int iteration = 1; iteration <= num; iteration++) {
                        variable.setValue(ans);
                        ans = evaluatePostfix(exp, variables);
                    }
                    answerStack.push(String.valueOf(ans));
                } else if (name.equalsIgnoreCase("determinant")) {
                    for (Matrix2D matrix2D : Objects.globalMatrices2D) {
                        if (matrix2D.getName().equalsIgnoreCase(token.substring(token.indexOf(openBracket) + 1, token.lastIndexOf(closeBracket)).trim())) {
                            answerStack.push(String.valueOf(matrix2D.evaluateDeterminant()));
                            break;
                        }
                    }
                } else {
                    for (Function function : Objects.globalFunctions) {
                        if (function.getName().equalsIgnoreCase(token.substring(0, token.indexOf(openBracket)).trim())) {
                            answerStack.push(String.valueOf(function.evaluate(arguments)));
                            continue;
                        }
                    }
                }
            }
        }
        return Double.parseDouble(answerStack.pop());
    }
}
