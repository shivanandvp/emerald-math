# Emerald Math

A computer algebra system and graphing tool written in Java. Supports creation of variables, matrices, functions and plots (2D parameteric, 2D cartesian, 2D polar, 3D spherical, 3D cylindrical, and 3D cartesian).  
  
Demo: https://www.youtube.com/watch?v=IO-ciQQOo6Q  
  
(This project can be opened using the free [Netbeans IDE](https://netbeans.org/))